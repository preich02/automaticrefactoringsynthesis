package autoRefactor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import main.MainClass;
import main.Utils;
import main.Gridlang;
import main.LogBox;

public class ARGenerator {
	private static int param_nExamples = 10;
	public static boolean use_flat_operator_weightings = true;


	public static void printSpaceSize() {
		Gridlang lang = new Gridlang();
		System.out.println(lang.getMaximalOptions(ARSearch.nVars, ARSearch.nArrays).size());
	}

	public static void generate(boolean useCatStrat){
		long seed =Utils.random.nextLong();
		LogBox seedLog = new LogBox("SeedLog");
		seedLog.takeLine("Corpus seeed: " + seed);
		Utils.random.setSeed(seed);;

		long time = System.currentTimeMillis();
		ArrayList<int[][]> bulkPrograms  = null;
		if (useCatStrat) {
			bulkPrograms = ARExploreByMutation.generateAndReturnTrainingSet_catStrat(10000,10000);
		}else {
			ARExploreByMutation.param_nCorpusRebuilds = 0;
			bulkPrograms = ARExploreByMutation.generateAndReturnTrainingSet(-1,500,500);

		}
		ArrayList<int[][]> trainPrograms = new ArrayList<int[][]>();
		ArrayList<int[][]> testPrograms = new ArrayList<int[][]>();
		ArrayList<int[][]> valPrograms = new ArrayList<int[][]>();
		ArrayList<int[][]> trainProgramsSub = new ArrayList<int[][]>();
		ArrayList<int[][]> testProgramsSub = new ArrayList<int[][]>();
		ArrayList<int[][]> valProgramsSub = new ArrayList<int[][]>();
		int splitLine = (int) (bulkPrograms.size()*0.8);//Most go to training, val corpus is smaller
		time = System.currentTimeMillis() - time;
		System.out.println("Corpus generated. Time taken: " + Utils.commas(time) + " ms");

		Collections.shuffle(bulkPrograms);

		trainProgramsSub.addAll(bulkPrograms.subList(0, splitLine));
		ArrayList<int[][]> remainderCorpus = new ArrayList<int[][]>();
		remainderCorpus.addAll(bulkPrograms.subList(splitLine, bulkPrograms.size()));
		valProgramsSub.addAll(remainderCorpus.subList(0, remainderCorpus.size()/2));
		testProgramsSub.addAll(remainderCorpus.subList(remainderCorpus.size()/2,remainderCorpus.size()));

		System.out.println("Writing out programs. Reseed flag: " + ARExploreByMutation.param_useIteratedSeedPrograms);

		LogBox log_programs_training = new LogBox("master_humanReadablePrograms_training");
		int q = 0;
		for (int[][] program:trainProgramsSub){
			log_programs_training.takeLine("----Program " + q + "-----");
			for (int[] line:program){
				String s = "";
				for (int i:line){
					s += i + " ";
				}
				log_programs_training.takeLine(s);
			}
			q += 1;
		}

		q = 0;
		LogBox log_programs_val = new LogBox("master_humanReadablePrograms_val");
		for (int[][] program:valProgramsSub){
			log_programs_val.takeLine("----Program " + q + "-----");
			for (int[] line:program){
				String s = "";
				for (int i:line){
					s += i + " ";
				}
				log_programs_val.takeLine(s);
			}
			q += 1;
		}

		if (ARExploreByMutation.param_useIteratedSeedPrograms == false){
			q = 0;
			LogBox log_programs_testing = new LogBox("master_humanReadablePrograms_testing");
			for (int[][] program:testProgramsSub){
				log_programs_testing.takeLine("----Program " + q + "-----");
				for (int[] line:program){
					String s = "";
					for (int i:line){
						s += i + " ";
					}
					log_programs_testing.takeLine(s);
				}
				q += 1;
			}
		}

		for (int[][] program:trainProgramsSub){
			for (int r = 0;r<ARSearch.param_repeatsOfPrograms;r++){
				trainPrograms.add(program);
			}
		}
		for (int[][] program:valProgramsSub){
			for (int r = 0;r<ARSearch.param_repeatsOfPrograms;r++){
				valPrograms.add(program);
			}
		}

		if (ARExploreByMutation.param_useIteratedSeedPrograms == false){
			for (int[][] program:testProgramsSub){
				for (int r = 0;r<ARSearch.param_repeatsOfPrograms;r++){
					testPrograms.add(program);
				}
			}
		}

		Gridlang lang = new Gridlang();
		ArrayList<int[]> opts = lang.getMaximalOptions(ARSearch.nVars, ARSearch.nArrays);
		LogBox log_training = new LogBox("master_labels_training");
		LogBox features_training = new LogBox("features_training");
		LogBox features_training_scalars = new LogBox("features_training_scalars");
		q = 0;
		for (int[][] program:trainPrograms){
			String labelString = "";
			for (int i=0;i<program.length;i++){
				int index = ARSearch.getIndexOfLine(program[i],opts);
				labelString += index + " ";
			}
			labelString = labelString.substring(0, labelString.length() - 1);
			log_training.takeLine(labelString);
			saveProgramToDisk_Binary(features_training,program,lang,q);
			saveProgramToDisk(features_training_scalars,program,lang,q);
			q += 1;
		}

		q = 0;
		LogBox log_val = new LogBox("master_labels_val");
		LogBox features_val = new LogBox("features_val");
		LogBox features_val_scalars = new LogBox("features_val_scalars");
		for (int[][] program:valPrograms){
			String labelString = "";
			for (int i=0;i<program.length;i++){
				int index = ARSearch.getIndexOfLine(program[i],opts);
				labelString += index + " ";
			}
			labelString = labelString.substring(0, labelString.length() - 1);
			log_val.takeLine(labelString);
			saveProgramToDisk_Binary(features_val,program,lang,q);
			saveProgramToDisk(features_val_scalars,program,lang,q);
			q += 1;
		}

		if (ARExploreByMutation.param_useIteratedSeedPrograms == false){
			q = 0;
			LogBox log_testing = new LogBox("master_labels_testing");
			LogBox features_testing = new LogBox("features_testing");
			LogBox features_testing_scalars = new LogBox("features_testing_scalars");
			for (int[][] program:testPrograms){
				String labelString = "";
				for (int i=0;i<program.length;i++){
					int index = ARSearch.getIndexOfLine(program[i],opts);
					labelString += index + " ";
				}
				labelString = labelString.substring(0, labelString.length() - 1);
				log_testing.takeLine(labelString);
				saveProgramToDisk_Binary(features_testing,program,lang,q);
				saveProgramToDisk(features_testing_scalars,program,lang,q);
				q += 1;
			}
		}

		LogBox log_humanUseful = new LogBox("master_labels_human");
		LogBox features_humanUseful = new LogBox("features_human");
		LogBox features_humanUseful_scalars = new LogBox("features_human_scalars");
		q = 0;
		for (int[][] program:MainClass.getP3Set(ARSearch.nLines)){
			String labelString = "";
			for (int i=0;i<program.length;i++){
				int index = ARSearch.getIndexOfLine(program[i],opts);
				labelString += index + " ";
			}
			labelString = labelString.substring(0, labelString.length() - 1);
			log_humanUseful.takeLine(labelString);
			saveProgramToDisk_Binary(features_humanUseful,program,lang,q);
			saveProgramToDisk(features_humanUseful_scalars,program,lang,q);
			q += 1;
		}
	}


	public static double getDistanceFromCorpus(int[][] programA, ArrayList<int[][]> set) {
		double minDist = -1;
		for (int[][] programB:set) {
			double dist =  getDistance(programA, programB);
			if (dist < minDist || minDist == -1) {
				minDist = dist;
			}
		}
		return minDist;
	}

	public static float getDistance(int[][] a,int[][] b){
		float dist = 0;
		for (int i=0;i<a.length;i++){
			for (int j=0;j<a[0].length;j++){
				if (a[i][j] != b[i][j]){
					dist += 1;
				}
			}
		}
		return dist;
	}

	public static ArrayList<int[][]> loadReseedPrograms(){
		ArrayList<String> fileLabels = new ArrayList<String>();
		File file = new File("reseedPrograms.log");
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));

			String st;
			while ((st = br.readLine()) != null) {
				fileLabels.add(st);
			}
			br.close();
		}catch(Exception e) {
			e.printStackTrace()
			;
		}
		Gridlang lang = new Gridlang();
		ArrayList<int[]> opts = lang.getMaximalOptions(ARSearch.nVars, ARSearch.nArrays);
		ArrayList<int[]> targetLabels = new ArrayList<int[]>();
		for (int i=0;i<fileLabels.size();i++){
			targetLabels.add(toIntArray(fileLabels.get(i)));
		}

		ArrayList<int[][]> programs = new ArrayList<int[][]>();
		for (int[] labels:targetLabels){
			int[][] program = new int[labels.length][];
			for (int i=0;i<labels.length;i++){
				program[i] = opts.get(labels[i]);
			}
			programs.add(program);
		}

		return programs;
	}
	public static ArrayList<int[][]> loadProgramsFromDisk(){
		ArrayList<String> fileLabels = new ArrayList<String>();
		File file = new File("master_labels_training.log");
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));

			String st;
			while ((st = br.readLine()) != null) {
				fileLabels.add(st);
			}
			br.close();
		}catch(Exception e) {
			e.printStackTrace()
			;
		}
		ArrayList<String> fileLabelsTest = new ArrayList<String>();
		File fileTest = new File("master_labels_testing.log");
		try {
			BufferedReader br = new BufferedReader(new FileReader(fileTest));

			String st;
			while ((st = br.readLine()) != null) {
				fileLabelsTest.add(st);
			}
			br.close();
		}catch(Exception e) {
			e.printStackTrace()
			;
		}
		Gridlang lang = new Gridlang();
		ArrayList<int[]> opts = lang.getMaximalOptions(ARSearch.nVars, ARSearch.nArrays);
		ArrayList<int[]> targetLabels = new ArrayList<int[]>();
		for (int i=0;i<fileLabels.size();i++){
			targetLabels.add(toIntArray(fileLabels.get(i)));
		}

		ArrayList<int[][]> programs = new ArrayList<int[][]>();
		for (int[] labels:targetLabels){
			int[][] program = new int[labels.length][];
			for (int i=0;i<labels.length;i++){
				program[i] = opts.get(labels[i]);
			}
			programs.add(program);
		}

		return programs;
	}

	public static int[] toIntArray(String str) {
		String[] split = str.split(" ");
		int[] reply = new int[split.length];
		for (int i=0;i<split.length;i++) {
			reply[i] = Integer.parseInt(split[i]);
		}

		return reply;
	}

	public static void saveProgramToDisk_Binary_Perturbated(LogBox logbox,
			int[][] program, Gridlang lang,int programIndex) {
		Random r = new Random();
		r.setSeed(programIndex);

		String desc = "";
		for (int e=0;e<param_nExamples  ;e++){
			int[] input = new int[8];
			for (int i=0;i<input.length;i++){
				input[i] = r.nextInt(16) - 8;
			}
			int param = r.nextInt(4) + 1;
			int[] output = new int[8];
			//			try{
			output = lang.process(program, ARSearch.nArrays, ARSearch.nVars, input, param);
			//			}catch(Exception exc){
			//				//Very bad to hit here, but worse to completely fall apart
			//				System.out.println("Major error: flawed program in corpus");
			//			}
			int q = 0;
			for (int i:input){
				double[] enc = Utils.intToBinaryOverflowable(i, 10);
				for (double d:enc){
					desc += d + " ";
				}
				desc += q + " ";
				q += 1;
			}
			double[] encP = Utils.intToBinaryOverflowable(param, 10);
			for (double d:encP){
				desc += d + " ";
			}
			q = 0;
			for (int i:output){
				double[] enc = Utils.intToBinaryOverflowable(i, 10);
				for (double d:enc){
					desc += d + " ";
				}
				desc += q + " ";
				q += 1;
			}
		}
		desc = desc.substring(0, desc.length()-1);
		logbox.takeLine(desc);
	}
	public static void saveProgramToDisk_Binary(LogBox logbox,
			int[][] program, Gridlang lang,int programIndex) {
		Random r = new Random();
		r.setSeed(programIndex);

		String desc = "";
		for (int e=0;e<param_nExamples  ;e++){
			int[] input = new int[8];
			for (int i=0;i<input.length;i++){
				input[i] = r.nextInt(16) - 8;
			}
			int param = r.nextInt(4) + 1;
			int[] output = new int[8];
			//			try{
			output = lang.process(program, ARSearch.nArrays, ARSearch.nVars, input, param);
			//			}catch(Exception exc){
			//				//Very bad to hit here, but worse to completely fall apart
			//				System.out.println("Major error: flawed program in corpus");
			//			}
			for (int i:input){
				double[] enc = Utils.intToBinary_Ceiling(i, 10);
				for (double d:enc){
					desc += d + " ";
				}
			}
			double[] encP = Utils.intToBinary_Ceiling(param, 10);
			for (double d:encP){
				desc += d + " ";
			}
			for (int i:output){
				double[] enc = Utils.intToBinary_Ceiling(i, 10);
				for (double d:enc){
					desc += d + " ";
				}
			}
		}
		desc = desc.substring(0, desc.length()-1);
		logbox.takeLine(desc);
	}

	public static void saveProgramToDisk(LogBox logbox,
			int[][] program, Gridlang lang,int programIndex) {
		Random r = new Random();
		r.setSeed(programIndex);

		String desc = "";
		for (int e=0;e<param_nExamples  ;e++){
			int[] input = new int[8];
			for (int i=0;i<input.length;i++){
				input[i] = r.nextInt(16) - 8;
			}
			int param = r.nextInt(4) + 1;
			int[] output = new int[8];
			//			try{
			output = lang.process(program, ARSearch.nArrays, ARSearch.nVars, input, param);
			//			}catch(Exception exc){
			//				//Very bad to hit here, but worse to completely fall apart
			//				System.out.println("Major error: flawed program in corpus");
			//			}
			for (int i:input){
				desc += i + " ";
			}
			desc += param + " ";
			for (int i:output){
				desc += i + " ";
			}
		}
		desc = desc.substring(0, desc.length()-1);
		logbox.takeLine(desc);
	}

	public static void deleteExistingTrainingFiles() {
		File f = new File("training_featureSets.log");
		if (f.exists()){f.delete();}
		f = new File("training_labels.log");
		if (f.exists()){f.delete();}

	}
}
