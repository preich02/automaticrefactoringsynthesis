package autoRefactor;

import java.util.Comparator;

public class IndexArrayComparator implements Comparator<double[]>{

	@Override
	public int compare(double[] o1, double[] o2) {
		return Double.compare(o2[0], o1[0]);
	}

}
