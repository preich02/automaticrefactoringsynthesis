package main;

import java.util.ArrayList;

public class Translator {

	public static void gridlangToJava(int[][] program) {
		for (int[] line:program){
			String strLine = "";
			for (int k:line){
				strLine += k + " ";
			}
			System.out.println(strLine);
		}
		System.out.println();

		ArrayList<String> output = new ArrayList<String>();
		output.add("static int nArrays = 2;");
		output.add("static int nVars = 9;");
		output.add("public static int[] generatedProgram(int[] inputArray,int param){" );
		output.add("int[][] arrays = new int[nArrays][inputArray.length];");
		output.add("arrays[0] = inputArray;");
		output.add("int[] variables = new int[nVars];");
		output.add("variables[0] = inputArray.length;");
		output.add("variables[1] = param;");
		for (int[] line:program){
			output.add(processLine(line));
		}
		output.add("return arrays[1];");
		int openBrackets = 0;
		for (String line:output){
			if (line.contains("{")){openBrackets += 1;}
			if (line.contains("}")){openBrackets -= 1;}
		}
		for (int i=0;i<openBrackets;i++){
			output.add("}");
		}

		System.out.println("Compiled program:");
		for (String line:output){
			System.out.println(line);
		}
	}

	private static String processLine(int[] line) {
		String strLine = "";
		for (int k:line){
			strLine += k + " ";
		}

		if (line[0] < 0){
			return processFlowOperator(line);
		}
		else if (line[0] == 0){
			return "";
		}
		else if (line[0] < 50){
			return processArithmeticOperator(line);
		}
		else if (line[0] < 100){
			return processArrayOp(line);
		}
		return "UNRECOGNISED LINE: " + strLine;
	}

	private static String processArrayOp(int[] line) {
		String strLine = "";
		for (int k:line){
			strLine += k + " ";
		}

		if (line[0] == Gridlang.OP_ARRAY_TO_VAR_ASSIGN){
			return "variables[" + line[3] + "] = arrays[" + line[1] +"][variables[" + line[2] + "]];";
		}
		else if (line[0] == Gridlang.OP_VAR_TO_ARRAY_ASSIGN){
			return "arrays[" + line[1] + "][variables[" + line[2] + "]] = variables[" + line[3] +"];";
		}
		else if (line[0] == Gridlang.OP_ARRAY_TO_ARRAY_ASSIGN){
			return "arrays[" + line[1] + "][variables[" + line[2] + "]] = arrays[" + line[3] + "][variables[" + line[4] +"]];";
		}
		return "UNRECOGNISED LINE: " + strLine;
	}

	private static String processArithmeticOperator(int[] line) {
		String strLine = "";
		for (int k:line){
			strLine += k + " ";
		}

		if (line[0] == Gridlang.OP_VAR_TO_LITERAL){
			return "variables[" + line[1] + "] = " + line[2] + ";"; 
		}
		else if (line[0] == Gridlang.OP_ADD){
			return "variables[" + line[1] + "] = variables[" + line[2] +"] + variables[" + line[3] + "];";
		}
		else if (line[0] == Gridlang.OP_SUBTRACT){
			return "variables[" + line[1] + "] = variables[" + line[2] +"] - variables[" + line[3] + "];";
		}
		else if (line[0] == Gridlang.OP_MULTIPLY){
			return "variables[" + line[1] + "] = variables[" + line[2] +"] * variables[" + line[3] + "];";
		}
		else if (line[0] == Gridlang.OP_MODULO){
			String reply = "if (variables[" + line[3] + "] == 0){variables[" + line[1] + "] = 0;}";
			reply += "else{variables[" + line[1] + "] = variables[" + line[2] +"] % variables[" + line[3] + "];}";
			return reply;}
		else if (line[0] == Gridlang.OP_DIVIDE){
			String reply = "if (variables[" + line[3] + "] == 0){variables[" + line[1] + "] = 0;}";
			reply += "else{variables[" + line[1] + "] = variables[" + line[2] +"] / variables[" + line[3] + "];}";
			return reply;
		}
		else if (line[0] == Gridlang.OP_INCREMENT){
			return "variables[" + line[1] + "] += 1;";
		}
		return "UNRECOGNISED LINE: " + strLine;
	}

	private static String processFlowOperator(int[] line) {
		String strLine = "";
		for (int k:line){
			strLine += k + " ";
		}

		if (line[0] == Gridlang.OP_ENDBLOCK){
			return "}";
		}
		else if (line[0] == Gridlang.OP_LOOP){
			return "for (variables[" + line[1] + "]=0;variables[" + line[1] + "]<variables["+line[2]+"];variables[" + line[1] + "]++){";
		}
		else if (line[0] == Gridlang.OP_COND){
			return "if (variables[" + line[1] + "]>0){";
		}
		else if (line[0] == Gridlang.OP_ELSEBLOCK){
			return "}else{";
		}
		else if (line[0] == Gridlang.OP_COND_EQUALS){
			return "if (variables[" + line[1] + "]==variables[" + line[2] + "]){";
		}

		return "UNRECOGNISED LINE: " + strLine;
	}
	static int nArrays = 2;
	static int nVars = 9;
	public static int[] generatedProgram(int[] inputArray,int param){
	int[][] arrays = new int[nArrays][inputArray.length];
	arrays[0] = inputArray;
	int[] variables = new int[nVars];
	variables[0] = inputArray.length;
	variables[1] = param;
	for (variables[2]=0;variables[2]<variables[0];variables[2]++){
	variables[3] = arrays[0][variables[2]];
	variables[4] = variables[4] + variables[3];
	}
	arrays[1][variables[5]] = variables[4];




	return arrays[1];
	}
}
