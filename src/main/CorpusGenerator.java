package main;

import java.util.ArrayList;
import java.util.Collections;

import autoRefactor.ARSearch;
import problems.PEasy_AddOne;
import problems.Problem;

public class CorpusGenerator {
	private static int featureLength;



	public static ArrayList<int[][]> generateProgramCorpus(int nToFind,int nLines,int nArrays,int nVars) {
		Gridlang interpreter = new Gridlang();
		int[][] program = new int[nLines][];
		Problem p = new PEasy_AddOne();

		long seed = Utils.random.nextLong();
		//		seed = 9098232095238450278l;
		System.out.println(seed);
		Utils.random.setSeed(seed);

		ArrayList<int[]> maximalOpts = interpreter.getMaximalOptions(nVars, nArrays);
		double magnitude = 0;
		ArrayList<ArrayList<int[]>> optsPerLine = new ArrayList<ArrayList<int[]>>();
		for (int line=0;line<program.length;line++) {
			//			ArrayList<int[]> opts = interpreter.getMaximalOptions(nVars, nArrays);
			ArrayList<int[]> opts = interpreter.getLineOptions(program,line,nVars, nArrays);
			optsPerLine.add(opts);
			double magPerLine = Math.log10(opts.size());
			magnitude += magPerLine;
			System.out.print(opts.size() + " ");
		}
		System.out.println();
		System.out.println("Estimated order of magnitude of search space: " + magnitude);

		int[] currentLocation = new int[nLines];
		ArrayList<int[][]> programs = new ArrayList<int[][]>();

		long seekCount = 0;
		masterLoop:
			while(true) {
				seekCount += 1;
				if (seekCount % 100000 == 0) {
					System.out.println("Seek count: " + Utils.commas(seekCount) + " found: " + programs.size()
					+ " FeatureLength: " + featureLength + " nOptsMaximally " + maximalOpts.size());
				}
				program = new int[nLines][];
				for (int i=0;i<currentLocation.length;i++) {
					currentLocation[i] = Utils.random.nextInt(optsPerLine.get(i).size());
				}
				for (int i=0;i<currentLocation.length;i++) {
					program[i] = optsPerLine.get(i).get(currentLocation[i]);
				}

				boolean containsControl = false;
				for (int i=0;i<program.length;i++) {
					if (program[i][Gridlang.OP] < 0) {
						containsControl = true;
					}
				}
				if (!containsControl) {continue masterLoop;}

				boolean anyMatch = false;
				try {
					int[] input = p.randomInput(8);
					int param = Utils.random.nextInt(4) + 1;
					int[] out = interpreter.process(program, nArrays, nVars, input.clone(), param);

					boolean divergence = false;
					int[] secondInput = p.randomInput(8);
					int secondParam = Utils.random.nextInt(4) + 1;
					int[] secondOut = interpreter.process(program, nArrays, nVars, secondInput.clone(), secondParam);

					for (int i=0;i<secondOut.length;i++) {
						if (secondOut[i] != out[i]) {
							divergence = true;
							break;
						}
					}
					if (divergence == false) {continue masterLoop;}

					for (int[][] other:programs) {
						int[] compareOut = interpreter.process(other, nArrays, nVars, input.clone(), param);
						int[] compareOut2 = interpreter.process(other, nArrays, nVars, secondInput.clone(), secondParam);

						divergence = false;
						for (int i=0;i<compareOut.length;i++) {
							if (compareOut[i] != out[i]) {
								divergence = true;
								break;
							}
							if (compareOut2[i] != secondOut[i]) {
								divergence = true;
								break;
							}
						}
						if (!divergence) {
							anyMatch = true;
							break;
						}
					}
				}catch(Exception e) {
					//Nothing
					anyMatch = true;
				}catch(Error e) {
					anyMatch = true;
				}

				if (!anyMatch) {
					program = ablate(program,p,nArrays,nVars);

					//Ablation can return null if it discovers the original program is flawed
					if (program == null) {
						continue masterLoop;
					}

					program = refactorVariableNaming(program,p,nArrays,nVars);
					//Renaming can return null if it discovers the original program is flawed
					if (program == null) {
						continue masterLoop;
					}

					//Recheck it contains a control statement now that you've ablated
					//					containsControl = false;
					boolean hasConditional = false;
					boolean hasLoop = false;
					boolean hasLoopEnd = false;
					for (int i=0;i<program.length;i++) {
						//						if (program[i][Gridlang.OP] < 0) {
						//							containsControl = true;
						//						}
						if (program[i][Gridlang.OP] == Gridlang.OP_COND) {
							hasConditional = true;
						}
						if (program[i][Gridlang.OP] == Gridlang.OP_LOOP || program[i][Gridlang.OP] == Gridlang.OP_WHILE ) {
							hasLoop = true;
						}
						if (program[i][Gridlang.OP] == Gridlang.OP_ENDBLOCK) {
							hasLoopEnd = true;
						}
					}

					containsControl = (hasConditional) || (hasLoop && hasLoopEnd);
					if (!containsControl) {continue masterLoop;}


					boolean passedChecking = true;
					try {
						for (int q=0;q<25;q++) {
							int[] input = p.randomInput(8);
							int param = Utils.random.nextInt(4) + 1;
							interpreter.process(program, nArrays, nVars, input.clone(), param);
						}
					}catch(Exception e) {
						passedChecking = false;
					}

					if (!passedChecking) {continue masterLoop;}
					//A novel program has been generated
					programs.add(program);

					if (programs.size() >= nToFind ) {
						System.out.println("Program seek complete");
						break masterLoop;
					}
				}
			}

		return programs;
	}
	
	public static void printByIndex() {
		Gridlang interpreter = new Gridlang();
		int nArrays = 0;
		int nVars = 3;
		ArrayList<int[]> maximalOpts = interpreter.getMaximalOptions(nVars, nArrays);

		for (int i=0;i<maximalOpts.size();i++) {
			String str = "Index " + i + " : ";
			for (int q:maximalOpts.get(i)) {
				str += q + " ";
			}
			System.out.println(str);
		}
	}

	public static void displayRandomGridlangs(int nToFind,int nArrays,int nVars,int len) {
		Gridlang interpreter = new Gridlang();
		int[][] program = new int[len][];
		Problem p = new PEasy_AddOne();

		long seed = Utils.random.nextLong();
		//		seed = 9098232095238450278l;
		System.out.println(seed);
		Utils.random.setSeed(seed);

		double magnitude = 0;
		ArrayList<ArrayList<int[]>> optsPerLine = new ArrayList<ArrayList<int[]>>();
		for (int line=0;line<program.length;line++) {
			//			ArrayList<int[]> opts = interpreter.getMaximalOptions(nVars, nArrays);
			ArrayList<int[]> opts = interpreter.getLineOptions(program,line,nVars, nArrays);
			optsPerLine.add(opts);
			double magPerLine = Math.log10(opts.size());
			magnitude += magPerLine;
			System.out.print(opts.size() + " ");
		}
		System.out.println();
		System.out.println("Estimated order of magnitude of search space: " + magnitude);
		int nFound = 0;
		int nTried = 0;
		int nFails = 0;
			while(nFound < nToFind){
				nTried += 1;
				if (nTried % 100 == 0){
					System.out.println("nTried: " + nTried + " found: " + nFound + " fails: " + nFails);
				}
				int[] currentLocation = new int[program.length];
				program = new int[currentLocation.length][];
				for (int i=0;i<currentLocation.length;i++) {
					currentLocation[i] = Utils.random.nextInt(optsPerLine.get(i).size());
				}
				for (int i=0;i<currentLocation.length;i++) {
					program[i] = optsPerLine.get(i).get(currentLocation[i]);
				}
				//Invalids must be removed
				if (!checkStability(program,nVars,nArrays,interpreter)){nFails += 1;continue;}
				//Ablate away useless lines
				int[][] oldProgram = program;
				program = ablateFast(program,p,nArrays,nVars);

				//Too slow for exploration. Can be used once you've got a full set of programs
				//It is theoretically better than the fast ablate, but empirically just a waste of time
//				 time time = System.currentTimeMillis();
//				program = ablate(program,p,nArrays,nVars);
//				time = System.currentTimeMillis() - time;
//				System.out.println("Time taken to ablate: " + time + "ms");

				//Program was flawed
				if (program == null) {continue;}
				
				boolean validAblation = ARSearch.areFunctionallyIdentical(oldProgram,program,nVars,nArrays,interpreter);
				if (!validAblation){throw new Error();}

				int nLinesUsed = 0;
				for (int i=0;i<program.length;i++){
					if (program[i][0] != 0){nLinesUsed += 1;}
				}
				if (nLinesUsed < 5){continue;}
				System.out.println(nLinesUsed);
				nFound += 1;
			}
	}

	public static boolean checkStability(int[][] program,int nVars,int nArrays,Gridlang interpreter) {
		Problem p = new PEasy_AddOne();
		try {
			for (int t=0;t<250;t++) {
				int[] input = p.randomInput(8);
				int param = Utils.random.nextInt(4) + 1;
				interpreter.process(program, nArrays, nVars, input.clone(), param);
			}
		}catch(java.lang.ArrayIndexOutOfBoundsException e) {
			return false;
		}
		return true;
	}

	public static ArrayList<int[][]> seekInterestingGridlang(int nToFind,int nArrays,int nVars) {
		Gridlang interpreter = new Gridlang();
		int[][] program = new int[6][];
		Problem p = new PEasy_AddOne();

		long seed = Utils.random.nextLong();
		//		seed = 9098232095238450278l;
		System.out.println(seed);
		Utils.random.setSeed(seed);

		ArrayList<int[]> maximalOpts = interpreter.getMaximalOptions(nVars, nArrays);
		double magnitude = 0;
		ArrayList<ArrayList<int[]>> optsPerLine = new ArrayList<ArrayList<int[]>>();
		for (int line=0;line<program.length;line++) {
			//			ArrayList<int[]> opts = interpreter.getMaximalOptions(nVars, nArrays);
			ArrayList<int[]> opts = interpreter.getLineOptions(program,line,nVars, nArrays);
			optsPerLine.add(opts);
			double magPerLine = Math.log10(opts.size());
			magnitude += magPerLine;
			System.out.print(opts.size() + " ");
		}
		System.out.println();
		System.out.println("Estimated order of magnitude of search space: " + magnitude);

		int[] currentLocation = new int[program.length];
		ArrayList<int[][]> programs = new ArrayList<int[][]>();

		long seekCount = 0;
		masterLoop:
			while(true) {
				seekCount += 1;
				if (seekCount % 100000 == 0) {
					System.out.println("Seek count: " + Utils.commas(seekCount) + " found: " + programs.size()
					+ " FeatureLength: " + featureLength + " nOptsMaximally " + maximalOpts.size());
				}
				program = new int[6][];
				for (int i=0;i<currentLocation.length;i++) {
					currentLocation[i] = Utils.random.nextInt(optsPerLine.get(i).size());
				}
				for (int i=0;i<currentLocation.length;i++) {
					program[i] = optsPerLine.get(i).get(currentLocation[i]);
				}

				boolean containsControl = false;
				for (int i=0;i<program.length;i++) {
					if (program[i][Gridlang.OP] < 0) {
						containsControl = true;
					}
				}
				if (!containsControl) {continue masterLoop;}

				boolean anyMatch = false;
				try {
					int[] input = p.randomInput(8);
					int param = Utils.random.nextInt(4) + 1;
					int[] out = interpreter.process(program, nArrays, nVars, input.clone(), param);

					boolean divergence = false;
					int[] secondInput = p.randomInput(8);
					int secondParam = Utils.random.nextInt(4) + 1;
					int[] secondOut = interpreter.process(program, nArrays, nVars, secondInput.clone(), secondParam);

					for (int i=0;i<secondOut.length;i++) {
						if (secondOut[i] != out[i]) {
							divergence = true;
							break;
						}
					}
					if (divergence == false) {continue masterLoop;}

					for (int[][] other:programs) {
						int[] compareOut = interpreter.process(other, nArrays, nVars, input.clone(), param);
						int[] compareOut2 = interpreter.process(other, nArrays, nVars, secondInput.clone(), secondParam);

						divergence = false;
						for (int i=0;i<compareOut.length;i++) {
							if (compareOut[i] != out[i]) {
								divergence = true;
								break;
							}
							if (compareOut2[i] != secondOut[i]) {
								divergence = true;
								break;
							}
						}
						if (!divergence) {
							anyMatch = true;
							break;
						}
					}
				}catch(Exception e) {
					//Nothing
					anyMatch = true;
				}catch(Error e) {
					anyMatch = true;
				}

				if (!anyMatch) {
					program = ablate(program,p,nArrays,nVars);

					//Ablation can return null if it discovers the original program is flawed
					if (program == null) {
						continue masterLoop;
					}

					program = refactorVariableNaming(program,p,nArrays,nVars);
					//Renaming can return null if it discovers the original program is flawed
					if (program == null) {
						continue masterLoop;
					}

					//Recheck it contains a control statement now that you've ablated
					//					containsControl = false;
					boolean hasConditional = false;
					boolean hasLoop = false;
					boolean hasLoopEnd = false;
					for (int i=0;i<program.length;i++) {
						//						if (program[i][Gridlang.OP] < 0) {
						//							containsControl = true;
						//						}
						if (program[i][Gridlang.OP] == Gridlang.OP_COND) {
							hasConditional = true;
						}
						if (program[i][Gridlang.OP] == Gridlang.OP_LOOP || program[i][Gridlang.OP] == Gridlang.OP_WHILE ) {
							hasLoop = true;
						}
						if (program[i][Gridlang.OP] == Gridlang.OP_ENDBLOCK) {
							hasLoopEnd = true;
						}
					}

					containsControl = (hasConditional) || (hasLoop && hasLoopEnd);
					if (!containsControl) {continue masterLoop;}


					boolean passedChecking = true;
					try {
						for (int q=0;q<25;q++) {
							int[] input = p.randomInput(8);
							int param = Utils.random.nextInt(4) + 1;
							interpreter.process(program, nArrays, nVars, input.clone(), param);
						}
					}catch(Exception e) {
						passedChecking = false;
					}

					if (!passedChecking) {continue masterLoop;}
					//A novel program has been generated
					programs.add(program);

					System.out.println();
					for (int q=0;q<10;q++) {
						int[] input = p.randomInput(8);
						int param = Utils.random.nextInt(4) + 1;
						int[] output = interpreter.process(program, nArrays, nVars, input.clone(), param);
						System.out.print("In: ");
						for (int vv=0;vv<input.length;vv++){
							System.out.print(input[vv] + " ");
						}
						System.out.println(" // " + param);
						System.out.print("Out: ");
						for (int vv=0;vv<input.length;vv++){
							System.out.print(output[vv] + " ");
						}
						System.out.println();
					}
					System.out.println();



					if (programs.size() >= nToFind ) {
						System.out.println("Program seek complete");
						break masterLoop;
					}
				}
			}

		//Programs have now been gathered, print them to disk and return them
		ArrayList<int[][]> trainingPrograms = new ArrayList<int[][]>();
		ArrayList<int[][]> testingPrograms = new ArrayList<int[][]>();
		Collections.shuffle(programs);
		int splitLine = (int) (programs.size()*0.75);
		trainingPrograms.addAll(programs.subList(0, splitLine));
		testingPrograms.addAll(programs.subList(splitLine, programs.size()));


		LogBox log = new LogBox("FoundPrograms_training","0");
		LogBox programLog = new LogBox("optCodePrograms_training","0");
		LogBox featureVectors = new LogBox("data_features_training","0");
		LogBox labels = new LogBox("data_labels_training","0");
		LogBox targetInputs = new LogBox("data_target_inputs_training","0");
		LogBox targetParams = new LogBox("data_target_params_training","0");
		LogBox targetOutputs = new LogBox("data_target_outputs_training","0");
		saveProgramsToDisk(log,programLog,featureVectors,labels,targetInputs,targetParams,targetOutputs,trainingPrograms,nArrays,nVars);

		log = new LogBox("FoundPrograms_testing","0");
		programLog = new LogBox("optCodePrograms_testing","0");
		featureVectors = new LogBox("data_features_testing","0");
		labels = new LogBox("data_labels_testing","0");
		targetInputs = new LogBox("data_target_inputs_testing","0");
		targetParams = new LogBox("data_target_params_testing","0");
		targetOutputs = new LogBox("data_target_outputs_testing","0");
		saveProgramsToDisk(log,programLog,featureVectors,labels,targetInputs,targetParams,targetOutputs,testingPrograms,nArrays,nVars);

		return programs;
	}

	public static void saveProgramsToDisk(
			LogBox logFoundPrograms,
			LogBox logOpCodes,
			LogBox featureVectors,
			LogBox logLabels,
			LogBox targetInputs,
			LogBox targetParams,
			LogBox targetOutputs,
			ArrayList<int[][]> programs,
			int nArrays,
			int nVars) {
		Gridlang interpreter = new Gridlang();
		ArrayList<int[]> maximalOpts = interpreter.getMaximalOptions(nVars, nArrays);
		PEasy_AddOne p = new PEasy_AddOne();

		for (int vv=0;vv<programs.size();vv++) {
			int[][] program = programs.get(vv);

			int passesPerProgram = 25;
			//			System.out.println();
			for (int pass=0;pass<passesPerProgram;pass++) {
				String featureVector = "";
				featureLength = 0;
				String outTargets = "";
				String inTargets = "";
				String paramTargets = "";
				for (int q=0;q<10;q++) {
					int[] input = p.randomInput(8);
					int param = Utils.random.nextInt(4) + 1;
					int[] out = interpreter.process(program, nArrays, nVars, input.clone(), param);
					for (int i=0;i<input.length;i++) {
						featureVector += input[i] + " ";
						featureLength  += 1;
					}
					featureVector += param + " ";
					featureLength += 1;
					for (int i=0;i<out.length;i++) {
						featureVector += out[i] + " ";
						featureLength += 1;
					}

					//					if (pass == 0) {
					//						for (int i:input) {
					//							System.out.print(i + " ");
					//						}
					//						System.out.println(" // " + param);
					//						for (int i:out) {
					//							System.out.print(i + " ");
					//						}
					//						System.out.println();
					//					}
					for (int i:input) {
						inTargets += i + " ";
					}
					for (int i:out) {
						outTargets += i + " ";
					}
					paramTargets += param + " ";
				}


				String optCode = "##########\n";
				for (int[] i:program) {
					for (int j:i) {
						optCode += j + ",";
					}
					optCode += "\n";
				}
				logOpCodes.takeLine(optCode);;

				//Get the current indices of the program
				int[] currentLocation = new int[program.length];
				for (int i=0;i<program.length;i++) {
					//Find the index for the label
					int[] lineData = program[i];
					int index = -1;
					outer:
						for (int o=0;o<maximalOpts.size();o++) {
							int[] opt = maximalOpts.get(o);
							for (int k=0;k<lineData.length;k++) {
								if (lineData[k] != opt[k]) {
									continue outer;
								}
							}
							//All passed
							index = o;
							break;
						}
					currentLocation[i] = index;
					if (index == -1) {
						System.out.println(maximalOpts.size());
						for (int[] line:maximalOpts) {
							for (int k=0;k<line.length;k++) {
								System.out.print(line[k] + " ");
							}
							System.out.println();
						}
						System.out.println("###########");
						for (int k=0;k<lineData.length;k++) {
							System.out.print(lineData[k] + " ");
						}
						System.out.println();
						throw new Error();
					}
				}

				String programString = "";
				for (int i:currentLocation){
					programString += i + " ";
				}

				for (int i=0;i<program.length;i++) {
					logFoundPrograms.takeLine(programString);
					featureVectors.takeLine(i + " " + featureVector);
					logLabels.takeLine("" + currentLocation[i]);
					targetInputs.takeLine(inTargets);;
					targetParams.takeLine(paramTargets);;
					targetOutputs.takeLine(outTargets);;
				}
			}
		}
	}


	/**
	 * Variables should be accessed in order. The lowest indexes first
	 * The first variable accessed should be the lowest free variable
	 * 
	 * This requires reshuffling values
	 * @param program
	 * @param p
	 * @param nArrays
	 * @param nVars
	 * @return
	 */
	public static int[][] refactorVariableNaming(int[][] program, Problem p, int nArrays, int nVars) {
		ArrayList<Integer> variablesUsed = new ArrayList<Integer>();
		ArrayList<ArrayList<int[]>> varUseLocations = new ArrayList<ArrayList<int[]>>();
		for (int i=0;i<nVars+Gridlang.builtinVars;i++) {
			varUseLocations.add(new ArrayList<int[]>());
		}
		ArrayList<ArrayList<int[]>> arrayUseLocations = new ArrayList<ArrayList<int[]>>();
		for (int i=0;i<nArrays+Gridlang.builtinArrays;i++) {
			arrayUseLocations.add(new ArrayList<int[]>());
		}

		int[] varFirstUse = new int[nVars+Gridlang.builtinVars];
		int[] arrayFirstUse = new int[nArrays+Gridlang.builtinArrays];
		for (int i=0;i<varFirstUse.length;i++) {
			varFirstUse[i] = 100000;
		}
		for (int i=0;i<arrayFirstUse.length;i++) {
			arrayFirstUse[i] = 100000;
		}
		for (int i=0;i<Gridlang.builtinVars;i++) {
			varFirstUse[i] = -1;
		}
		for (int i=0;i<Gridlang.builtinArrays;i++) {
			arrayFirstUse[i] = -1;
		}

		//		for (int[] opLine:program) {
		for (int line=0;line<program.length;line++) {
			int[] opLine = program[line];
			//No vars are touched on these lines
			if (opLine[0] == Gridlang.OP_END || opLine[0] == Gridlang.OP_ENDBLOCK ||
					opLine[0] == Gridlang.OP_ELSEBLOCK) {continue;}

			int[] definition = null;
			for (int[] opDef:Gridlang.allOperators) {
				if (opDef == null) {continue;}
				if (opDef[0] == opLine[0]) {
					definition = opDef;
				}
			}
			if (definition == null) {throw new Error(" " + opLine[0]);}

			if (opLine.length != 5){throw new Error("" + opLine.length);}
			for (int i=1;i<opLine.length;i++) {
				if (definition[i] == 0) {}
				else if (definition[i] == Gridlang.LIT) {}
				else if (definition[i] == Gridlang.VAR_READ || definition[i] == Gridlang.VAR_WRITE) {
//					//It's an array access/write
//					Integer arrayIndex = opLine[i];
//					if (arraysUsed.contains(arrayIndex) == false) {arraysUsed.add(arrayIndex);}
//					arrayUseLocations.get(opLine[i]).add(new int[] {line,i});
//					arrayFirstUse[opLine[i]] = Math.min(arrayFirstUse[opLine[i]], line*5 + i);
				}
				else if (definition[i] == Gridlang.AVAR_WRITE || definition[i] == Gridlang.AVAR_READ) {
					//It's a variable access/write
					Integer varIndex = opLine[i];
					if (variablesUsed.contains(varIndex) == false) {variablesUsed.add(varIndex);}
					varUseLocations.get(opLine[i]).add(new int[] {line,i});
					varFirstUse[opLine[i]] = Math.min(varFirstUse[opLine[i]], line*5 + i);
				}
				else {throw new Error(""+definition[i]);}
			}
		}


		//		System.out.println("Program:");
		//		for (int i=0;i<variablesUsed.size();i++) {
		//			System.out.println(i + " var: " + variablesUsed.get(i));
		//			for (int[] loc:varUseLocations.get(variablesUsed.get(i))) {
		//				System.out.println("   " + loc[0] + " " + loc[1]);
		//			}
		//		}

		int[][] shuffledProgram = clone(program);
		for (int q=0;q<varFirstUse.length;q++) {
			for (int i=1;i<varFirstUse.length;i++) {
				if (varFirstUse[i] < varFirstUse[i-1]) {
//					System.out.println("Swapping use of " + i + " and " + (i-1));
//					System.out.println("First uses: " + varFirstUse[i] + " and " + varFirstUse[(i-1)]);
//					System.out.println("Original: ");
//					for (int[] line:program) {
//						System.out.print("Line: ");
//						for (int k:line) {
//							System.out.print(k + " ");
//						}
//						System.out.println();
//					}

					//Could switch these two
					ArrayList<int[]> locationsOfA = varUseLocations.get(i);
					ArrayList<int[]> locationsOfB = varUseLocations.get(i-1);
					//Swap the two first uses
					int held = varFirstUse[i-1];
					varFirstUse[i-1] = varFirstUse[i];
					varFirstUse[i] = held;
					varUseLocations.set(i, locationsOfB);
					varUseLocations.set(i-1, locationsOfA);

					//Here is where A was used, replace it with B (i-1)
					for (int[] useOfA:locationsOfA) {
						shuffledProgram[useOfA[0]][useOfA[1]]  =   i-1;
					}
					//And similarly
					for (int[] useOfB:locationsOfB) {
						shuffledProgram[useOfB[0]][useOfB[1]]  =   i;
					}
//									System.out.println("Shuffled: ");
//									for (int[] line:shuffledProgram) {
//										System.out.print("Line: ");
//										for (int k:line) {
//											System.out.print(k + " ");
//										}
//										System.out.println();
//									}
//									for (int[] swap:locationsOfA) {
//										System.out.println("A: " + swap[0] + "," + swap[1]);
//									}
//									for (int[] swap:locationsOfB) {
//										System.out.println("B: " + swap[0] + "," + swap[1]);
//									}
					//				throw new Error();
				}
			}
		}
//		for (int q=0;q<arrayFirstUse.length;q++) {
//			for (int i=1;i<arrayFirstUse.length;i++) {
//				if (arrayFirstUse[i] < arrayFirstUse[i-1]) {
//					//Could switch these two
//					ArrayList<int[]> locationsOfA = arrayUseLocations.get(i);
//					ArrayList<int[]> locationsOfB = arrayUseLocations.get(i-1);
//					//Swap the two first uses
//					int held = arrayFirstUse[i-1];
//					arrayFirstUse[i-1] = arrayFirstUse[i];
//					arrayFirstUse[i] = held;
//					arrayUseLocations.set(i, locationsOfB);
//					arrayUseLocations.set(i-1, locationsOfA);
//
//					//Here is where A was used, replace it with B (i-1)
//					for (int[] useOfA:locationsOfA) {
//						shuffledProgram[useOfA[0]][useOfA[1]]  =   i-1;
//					}
//					//And similarly
//					for (int[] useOfB:locationsOfB) {
//						shuffledProgram[useOfB[0]][useOfB[1]]  =   i;
//					}
//				}
//			}
//		}

//		System.out.println("Refactor complete. Checking functionality match. a/v: " + nArrays + " " + nVars);
//		for (int[] line:program){
//			for (int i:line){
//				System.out.print(i + " ");
//			}
//		}
//		System.out.println();
//		for (int[] line:shuffledProgram){
//			for (int i:line){
//				System.out.print(i + " ");
//			}
//		}
//		System.out.println();
		if (p != null && (assertFunctionalities(p,program,shuffledProgram,nArrays,nVars) == false)) {
						throw new Error();
//			return null;
		}

		return shuffledProgram;
	}
	
	public static boolean assertFunctionalities(Problem p,int[][] program,
			int[][] shuffledProgram,int nArrayVars,int nVars) {
		
		Gridlang lang = new Gridlang();
		int[] input = new int[8];
		int param = 0;
		int[] outA;
		int[] outB;
		for (int t=0;t<25;t++) {
			for (int i=0;i<input.length;i++) {
				input[i] = Utils.random.nextInt(16) - 8;
			}
			param = 1 + Utils.random.nextInt(4);
			
			try{
				outA = lang.process(program, nArrayVars, nVars, input.clone(), param);
				outB = lang.process(shuffledProgram, nArrayVars, nVars, input.clone(), param);
				for (int i=0;i<outA.length;i++) {
					if (outA[i] != outB[i]) {
						return false;
					}
				}
			}catch(Exception e){
				return false;
			}
		}
		return true;
	}

	public static int[][] ablateFast(int[][] program,Problem p,int nArrayVars,int nVars) {
		Gridlang lang = new Gridlang();
		int nToTry = program.length + 1;
		nToTry *= nToTry;

		int deleteLineA = -1;
		int deleteLineB = -1;
		for (int cycle=0;cycle<nToTry;cycle++) {
			deleteLineA += 1;
			if (deleteLineA >= program.length) {
				deleteLineA = 0;
				deleteLineB += 1;
				if (deleteLineB >= program.length) {
					break;
				}
			}

			int nAblations = 0;
			int[][] ablated = clone(program);

			if (deleteLineA != -1 && program[deleteLineA][0] != Gridlang.OP_NOOP) {
				ablated[deleteLineA] = new int[] {Gridlang.OP_NOOP,0,0,0,0};
				nAblations += 1;
			}
			if (deleteLineB != -1 && program[deleteLineB][0] != Gridlang.OP_NOOP) {
				ablated[deleteLineB] = new int[] {Gridlang.OP_NOOP,0,0,0,0};
				nAblations += 1;
			}

			boolean changed = false;
			outer:
				for (int i=0;i<100;i++) {
					boolean failure = false;
					int[] input = p.randomInput(8).clone();
					int param = 1 + Utils.random.nextInt(4);
					int[] out = null;
					try {
						out = lang.process(program, nArrayVars, nVars, input, param);
					}catch (Exception e) {
						//The main program itself was flawed
						return null;
					}

					try {
						int[] newOut = lang.process(ablated, nArrayVars, nVars, input, param);

						for (int k=0;k<out.length;k++) {
							if (out[k] != newOut[k]) {
								changed = true;
								break outer;
							}
						}
					}catch(Exception e) {
						failure = true;
					}
					if (failure) {changed = true;break outer;}
				}
			//This version has been shown to be preferred, as it more ablated
			if (!changed) {
				program = ablated;
			}
		}

		//Shuffle all no-ops to the bottom
		for (int i=0;i<program.length;i++) {
			for (int j=1;j<program.length;j++) {
				if (program[j-1][Gridlang.OP] == Gridlang.OP_NOOP) {
					int[] held = program[j-1];
					program[j-1] = program[j];
					program[j] = held;
				}
			}
		}
		return program;
	}

	public static int[][] ablate(int[][] program,Problem p,int nArrayVars,int nVars) {
		Gridlang lang = new Gridlang();
		int bestNAblations = 0;

		int nToTry = 1;
		for (int i=0;i<program.length;i++) {
			nToTry *= 2;
		}

		//		System.out.println();
		//		System.out.println();
		for (int cycle=0;cycle<nToTry;cycle++) {
			int nAblations = 0;
			int[][] ablated = clone(program);
			int c = cycle;
			//			System.out.print(cycle + " ");
			for (int line = 0;line<program.length;line++) {
				if (c % 2 == 0) {
					ablated[line] = new int[] {Gridlang.OP_NOOP,0,0,0,0};
					nAblations += 1;
				}
				//				System.out.print(c % 2 + " ");
				c /= 2;
			}
			//			System.out.println();

			boolean changed = false;
			outer:
				for (int i=0;i<10;i++) {
					boolean failure = false;
					int[] input = p.randomInput(8).clone();
					int param = 1 + Utils.random.nextInt(4);
					int[] out = null;
					try {
						out = lang.process(program, nArrayVars, nVars, input, param);
					}catch (Exception e) {
						//The main program itself was flawed
						return null;
					}

					try {
						int[] newOut = lang.process(ablated, nArrayVars, nVars, input, param);

						for (int k=0;k<out.length;k++) {
							if (out[k] != newOut[k]) {
								changed = true;
								break outer;
							}
						}
					}catch(Exception e) {
						failure = true;
					}
					if (failure) {changed = true;break outer;}
				}
			//This version has been shown to be preferred, as it more ablated
			if (!changed && nAblations > bestNAblations) {
				program = ablated;
				bestNAblations = nAblations;
			}
		}

		//Shuffle all no-ops to the bottom
		for (int i=0;i<program.length;i++) {
			for (int j=1;j<program.length;j++) {
				if (program[j-1][Gridlang.OP] == Gridlang.OP_NOOP) {
					int[] held = program[j-1];
					program[j-1] = program[j];
					program[j] = held;
				}
			}
		}
		return program;
	}

	public static int[][] ablate_noReshuffle(int[][] program,Problem p,int nArrayVars,int nVars) {
		Gridlang lang = new Gridlang();
		int bestNAblations = 0;

		int nToTry = 1;
		for (int i=0;i<program.length;i++) {
			nToTry *= 2;
		}

		//		System.out.println();
		//		System.out.println();
		for (int cycle=0;cycle<nToTry;cycle++) {
			int nAblations = 0;
			int[][] ablated = clone(program);
			int c = cycle;
			//			System.out.print(cycle + " ");
			for (int line = 0;line<program.length;line++) {
				if (c % 2 == 0) {
					ablated[line] = new int[] {Gridlang.OP_NOOP,0,0,0,0};
					nAblations += 1;
				}
				//				System.out.print(c % 2 + " ");
				c /= 2;
			}
			//			System.out.println();

			boolean changed = false;
			outer:
				for (int i=0;i<10;i++) {
					boolean failure = false;
					int[] input = p.randomInput(8).clone();
					int param = 1 + Utils.random.nextInt(4);
					int[] out = null;
					try {
						out = lang.process(program, nArrayVars, nVars, input, param);
					}catch (Exception e) {
						//The main program itself was flawed
						return null;
					}

					try {
						int[] newOut = lang.process(ablated, nArrayVars, nVars, input, param);

						for (int k=0;k<out.length;k++) {
							if (out[k] != newOut[k]) {
								changed = true;
								break outer;
							}
						}
					}catch(Exception e) {
						failure = true;
					}
					if (failure) {changed = true;break outer;}
				}
			//This version has been shown to be preferred, as it more ablated
			if (!changed && nAblations > bestNAblations) {
				program = ablated;
				bestNAblations = nAblations;
			}
		}

		return program;
	}
	public static int[][] clone(int[][] in) {
		int[][] out = new int[in.length][in[0].length];
		for (int i=0;i<out.length;i++) {
			for (int x=0;x<out[0].length;x++) {
				out[i][x] = in[i][x];
			}
		}
		return out;
	}
}
