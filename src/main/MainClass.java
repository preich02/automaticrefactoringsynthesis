package main;

import humanUsefulProblems.P3_Abs;
import humanUsefulProblems.P3_AddOne;
import humanUsefulProblems.P3_AddParam;
import humanUsefulProblems.P3_ArrayLength;
import humanUsefulProblems.P3_Modulo2Values;
import humanUsefulProblems.P3_ModuloOfParam;
import humanUsefulProblems.P3_MultiplyByParam;
import humanUsefulProblems.P3_NegativeOfValues;
import humanUsefulProblems.P3_AllNegativesToZero;
import humanUsefulProblems.P3_CountParam;
import humanUsefulProblems.P3_IteratorUpToParam;
import humanUsefulProblems.P3_KeepAboveParam;
import humanUsefulProblems.P3_MaxValueOrParam;
import humanUsefulProblems.P3_MinValueOrParam;
import humanUsefulProblems.P3_OffsetByOne;
import humanUsefulProblems.P3_OffsetByParam;
import humanUsefulProblems.P3_ReturnMax;
import humanUsefulProblems.P3_ReturnMin;
import humanUsefulProblems.P3_Reverse;
import humanUsefulProblems.P3_SumValues;
import humanUsefulProblems.Problem3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import autoRefactor.ARExploreByMutation;
import autoRefactor.ARGenerator;
import autoRefactor.ARSearch;
import problems.*;

public class MainClass {
	public static void main(String[] args) {
		/*
		 * The order in which a search space is traversed may have some impact on automatic refactoring
		 * since AR uses the first found implementation as the "preferred" implementation
		 * 
		 * Empirically, it doesn't seem to have too much effect
		 */
		ARSearch.param_depthFirstSearch = false;//Note: Use breadth first if false
		/*
		 * Non-paper tests showed that biasing the fitness function towards the exact operator
		 * distributions seen in the HU programs had little effect, and meant that some manner
		 * of prior knowledge had to be embedded to generate them
		 * 
		 * Leaving the weightings as flat is far preferable, from a generality point of view
		 */
		ARGenerator.use_flat_operator_weightings = true;//Does almost nothing? Leave as flat
		/*
		 * Restrictions used to guide the corpus generation
		 */
		ARExploreByMutation.param_useRestriction_arrayAccess = true;
		ARExploreByMutation.param_useRestriction = true;
		ARExploreByMutation.param_useHeuristicCorpusGen = true;
		ARExploreByMutation.param_nRestrictions = 5;//5 max
		
		/**
		 * Generate a corpus, train the neural network
		 * then iteratively seek for solutions and re-write the training
		 * corpus with these newly found implementations, to perform AR.
		 */
		ARGenerator.generate(true);
		ARSearch.generateNNParameterFiles();
		
		for (int itr=1;itr<10;itr++){
			callMainTraining();
			ARSearch.singlePass(itr);
		}
	}

	private static void testTranslator() {
		int index = 19;
		Translator.gridlangToJava(getP3Set(9).get(index));

		int[] input = new int[8];
		for (int i=0;i<input.length;i++) {input[i] = Utils.random.nextInt(16) - 8;}
		int param = 1 + Utils.random.nextInt(4);
		int[] out = Translator.generatedProgram(input, param);
		String strIn = "";
		for (int k:input) {
			strIn += k + " ";
		}
		strIn += "| " + param;
		System.out.println(strIn);
		String strOut = "";
		for (int k:out) {
			strOut += k + " ";
		}
		System.out.println(strOut);
	}

	private static void computeLineAverages() {
		ArrayList<int[]> allPrograms = new ArrayList<int[]>();
		File folder = new File(".");
		for (File f:folder.listFiles()) {
			if (f.getName().contains("label") == false) {continue;}
			ArrayList<String> fileLabels = new ArrayList<String>();
			try {
				BufferedReader br = new BufferedReader(new FileReader(f));

				String st;
				while ((st = br.readLine()) != null) {
					fileLabels.add(st);
				}
				br.close();
			}catch(Exception e) {
				e.printStackTrace()
				;
			}
			for (int i=0;i<fileLabels.size();i++){
				allPrograms.add(ARSearch.toIntArray(fileLabels.get(i)));
			}
		}

		Gridlang lang = new Gridlang();
		ArrayList<int[]> allOpts = lang.getMaximalOptions(ARSearch.nVars, ARSearch.nArrays);

		System.out.println("Found " + allPrograms.size() + " programs. Found " + allOpts.size() + " line options");

		double[][] avrgs = new double[9][allOpts.size()];
		for (int[] program:allPrograms) {
			for (int i=0;i<9;i++) {
				avrgs[i][program[i]] += 1d/allPrograms.size();
			}
		}

		double avrgLength = 0;
		for (int[] program:allPrograms) {
			for (int op:program) {
				if (op != 0) {
					avrgLength += 1;
				}
			}
		}
		avrgLength /= allPrograms.size();
		System.out.println("Average program length: " + avrgLength);

		double lenStdDev = 0;
		for (int[] program:allPrograms) {
			double len = 0;
			for (int op:program) {
				if (op != 0) {
					len += 1;
				}
			}
			len -= avrgLength;
			len *= len;
			lenStdDev += len;
		}
		lenStdDev /= allPrograms.size();
		lenStdDev = Math.sqrt(lenStdDev);
		System.out.println("Length stdDev: " + lenStdDev);

		for (int line=0;line<9;line++) {
			double sumThiel = 0;
			double offset = 0.0001;
			double mean = (1d/avrgs[line].length)+offset;
			for (int i=0;i<avrgs[line].length;i++) {
				if (avrgs[line][i] + offset == 0) {continue;}
				sumThiel += ((avrgs[line][i]+offset)/mean)*(Math.log((avrgs[line][i]+offset)/mean));
			}
			double thiel = sumThiel/avrgs[line].length;
			System.out.println("Thiel line " + line + " : " + thiel);
		}

		for (int l=0;l<9;l++) {
			int unused=  0;
			for (int i=0;i<avrgs[l].length;i++) {
				if (avrgs[l][i] == 0) {unused += 1;}
			}
			System.out.println("n unused ops line " + l + " : " + unused);
		}
		for (int l=0;l<9;l++) {
			String topVals = "Line " + l + " top lines: ";
			for (int i=0;i<10;i++) {
				topVals += getNthHighest(avrgs[l],i) + " ";
			}
			System.out.println(topVals);
		}
		for (int l=0;l<9;l++) {
			String topVals = "Line " + l + " top opts: ";
			for (int i=0;i<10;i++) {
				//				int[] opcode = allOpts.get(getNthHighest(avrgs[l],i));
				//				String sub = "[";
				//				for (int k:opcode) {sub += k + ",";}
				//				sub += "]";
				//				topVals +=  sub + " ";
				topVals += allOpts.get(getNthHighest(avrgs[l],i))[0] + " ";
			}
			System.out.println(topVals);
		}
		for (int l=0;l<9;l++) {
			String topVals = "Line " + l + " top vals: ";
			for (int i=0;i<10;i++) {
				topVals += ("" + getNthHighestVal(avrgs[l],i)).substring(0, 5) + " ";
			}
			System.out.println(topVals);
		}
		for (int l=0;l<9;l++) {
			String topVals = "Line " + l + " zero proportion: ";
			topVals += (avrgs[l][0]+"").substring(0, 5);
			System.out.println(topVals);
		}
	}

	public static double getNthHighestVal(double[] line,int rank) {
		boolean[] seen = new boolean[line.length];
		int choice = -1;
		double val = 0;
		for (int nth=0;nth<=rank;nth++) {
			choice = -1;
			double high = 0;
			boolean first = true;
			for (int i=0;i<line.length;i++) {
				if (seen[i]) {continue;}
				if (line[i] > high || first) {
					high = line[i];
					choice = i;
					first = false;
				}
			}
			seen[choice] = true;
			val = high;
		}
		return val;
	}
	public static int getNthHighest(double[] line,int rank) {
		boolean[] seen = new boolean[line.length];
		int choice = -1;
		for (int nth=0;nth<=rank;nth++) {
			choice = -1;
			double high = 0;
			boolean first = true;
			for (int i=0;i<line.length;i++) {
				if (seen[i]) {continue;}
				if (line[i] > high || first) {
					high = line[i];
					choice = i;
					first = false;
				}
			}
			seen[choice] = true;
		}
		return choice;
	}

	public static ArrayList<Problem3> getP3Set(){
		ArrayList<Problem3> p3Set = new ArrayList<Problem3>();
		p3Set.add(new P3_Abs());//0
		p3Set.add(new P3_AddOne());
		p3Set.add(new P3_AddParam());
		p3Set.add(new P3_AllNegativesToZero());
		p3Set.add(new P3_ArrayLength());
		p3Set.add(new P3_CountParam());//5
		p3Set.add(new P3_IteratorUpToParam());
		p3Set.add(new P3_KeepAboveParam());
		p3Set.add(new P3_MaxValueOrParam());
		p3Set.add(new P3_MinValueOrParam());
		p3Set.add(new P3_Modulo2Values());//10
		p3Set.add(new P3_ModuloOfParam());
		p3Set.add(new P3_MultiplyByParam());
		p3Set.add(new P3_NegativeOfValues());
		p3Set.add(new P3_OffsetByOne());
		p3Set.add(new P3_OffsetByParam());//15
		p3Set.add(new P3_ReturnMax());
		p3Set.add(new P3_ReturnMin());
		p3Set.add(new P3_Reverse());
		p3Set.add(new P3_SumValues());

		return p3Set;
	}
	public static ArrayList<int[][]> getP3Set(int nLines){
		ArrayList<int[][]> p3Set = new ArrayList<int[][]>();
		for (Problem3 p3:getP3Set()){
			p3Set.add(p3.getGridlang(nLines));
		}

		return p3Set;
	}

	private static void callMainTraining() {
		System.out.println("Calling python training");
		Runtime rt = Runtime.getRuntime();
		try {
			Process p = rt.exec("python3 mainTraining.py");
			p.waitFor();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void set_singleArray(){
		//		Gridlang.builtinArrays = 1;
		//		Gridlang.finalArrays = 0;
		//		Gridlang.finalVars = 1;
		//		SingleArrayParams.nVars = 3;
		//		SingleArrayParams.nArrays = 1;
		//		SingleArrayParams.nLines = 6;
		//		SingleArray_CyclicProgramGenerator.deleteExistingTrainingFiles();
		//		SingleArray_CyclicProgramGenerator.generate();
		//		SingleArray_CyclicProgramGenerator.generateNLinesFile();
		//		SingleArray_CyclicProgramGenerator.generateNaiveWeightings();
		//		SingleArray_ARSearch.callTrainingScript();
		//		SingleArray_ARSearch.singlePass();
		//		SingleArray_ARSearch.singlePass_noReplace();
	}

	public static void printProgramIOExamples() {
		for (int r=0;r<10;r++) {
			Problem p = new P_Unleaf();
			Gridlang interpreter = new Gridlang();
			int[][] program = new int[6][];
			int nArrays = 0;
			int nVars = 3;

			int[] input = p.randomInput(8);
			int param = Utils.random.nextInt(4) + 1;
			input[0] = param;
			int[] out = p.run(input, param);

			System.out.print("Input:  ");
			for (int i:input) {
				System.out.print(Utils.pad(i) + " ");
			}
			System.out.println("  param (input[0]): " + param);
			System.out.print("Output: ");
			for (int i:out) {
				System.out.print(Utils.pad(i) + " ");
			}
			System.out.println();
			System.out.println();
		}
	}

	public static void exhaustiveGenGridlang() {
		Gridlang interpreter = new Gridlang();
		int[][] program = new int[6][];
		int nArrays = 0;
		int nVars = 3;
		Problem p = new PEasy_AddOne();

		double magnitude = 0;

		ArrayList<ArrayList<int[]>> optsPerLine = new ArrayList<ArrayList<int[]>>();
		for (int line=0;line<program.length;line++) {
			ArrayList<int[]> opts = interpreter.getRestrictedOptions(program, line, nVars, nArrays);
			optsPerLine.add(opts);
			double magPerLine = Math.log10(opts.size());
			magnitude += magPerLine;
		}
		System.out.println("Estimated order of magnitude of search space: " + magnitude);


		System.out.print("Options Per Line: ");
		for (int line=0;line<program.length;line++) {
			System.out.print(optsPerLine.get(line).size() + " ");
		}
		System.out.println();


		int[] currentLocation = new int[program.length];
		//		currentLocation[5] = 1;
		//		currentLocation[4] = 42;
		//		currentLocation[3] = 235;

		//-------------------------
		//This block just checks that the oracle could be rebuilt using these options
		ArrayList<int[]> oracle = getProgram_AddOne();
		if (oracle.size() != program.length) {throw new Error();}
		int[] oracleLocation = new int[program.length];
		for (int i=0;i<oracle.size();i++) {
			int[] or = oracle.get(i);

			boolean found = false;
			outer:
				for (int k=0;k<optsPerLine.get(i).size();k++) {
					for (int j=0;j<or.length;j++) {
						if (or[j] != optsPerLine.get(i).get(k)[j]) {
							continue outer;
						}
					}
					//Passed entirely
					oracleLocation[i] = k;
					found = true;
					break;
				}

			if (!found) {
				System.out.print("FAILED Oracle: ");
				for (int qq:or) {
					System.out.print(qq + " ");
				}
				System.out.println();
				throw new Error();
			}
		}
		System.out.print("Oracle: ");
		for (int i:oracleLocation) {
			System.out.print(i + " ");
		}
		System.out.println();
		//---------End oracle reconstruction-------




		long nEvaluated = 0;
		long nPasses = 0;
		double t = System.currentTimeMillis();
		exhaustionLoop:
			while(true) {
				try {
					for (int i=0;i<program.length;i++) {
						program[i] = optsPerLine.get(i).get(currentLocation[i]);
					}

					int[] input = p.randomInput(8);
					int param = Utils.random.nextInt(4) + 1;
					int[] targetOutput = p.run(input.clone(), param);
					int[] out = interpreter.process(program, nArrays, nVars, input.clone(), param);

					boolean passed = true;
					for (int i=0;i<out.length;i++) {
						if (targetOutput[i] != out[i]) {
							passed = false;
							break;
						}
					}
					if (passed) {//Do some deeper checks, just to be sure
						outer:
							for (int k=0;k<10;k++) {
								input = p.randomInput(8);
								param = Utils.random.nextInt(4) + 1;
								targetOutput = p.run(input.clone(), param);
								out = interpreter.process(program, nArrays, nVars, input.clone(), param);
								for (int i=0;i<out.length;i++) {
									if (targetOutput[i] != out[i]) {
										passed = false;
										break outer;
									}
								}
							}
					}
					if (passed) {
						nPasses += 1;

						int nL = -1;
						for (int[] line:program) {
							nL += 1;
							System.out.print("Line: " + nL + " : ");
							for (int i:line) {
								System.out.print(i + " ");
							}
							System.out.println();
						}
					}

				}catch(Exception e) {}

				nEvaluated += 1;
				if (nEvaluated % 1000000 == 0) {
					t = System.currentTimeMillis() - t;
					System.out.println("nEvaluated: " + Utils.commas(nEvaluated) + " Passes: " + nPasses
							+ " time(ms) " + t);
					for (int i:currentLocation) {
						System.out.print(i + " ");
					}
					System.out.println();

					t = System.currentTimeMillis();
				}
				//				for (int i=0;i<out.length;i++) {
				//					System.out.print(out[i] + " ");
				//				}
				//				System.out.println();

				currentLocation[0] += 1;
				for (int i=0;i<currentLocation.length;i++) {
					if (currentLocation[i] >= optsPerLine.get(i).size()) {
						currentLocation[i] = 0;
						if (i == currentLocation.length-1) {
							System.out.println("Reached end of loop");
							break exhaustionLoop;
						}
						currentLocation[i + 1] += 1;
					}
				}
			}
	}

	public static void randomGenGridlang() {
		Gridlang interpreter = new Gridlang();
		int[][] program = new int[7][];
		int nArrays = 0;
		int nVars = 2;
		Problem p = new PEasy_AddOne();


		double nOpsPerLine = interpreter.getLineOptions(program, 0, nVars, nArrays).size();
		double magPerLine = Math.log10(nOpsPerLine);
		double magnitude = magPerLine*program.length;
		System.out.println("Estimated order of magnitude of search space: " + magnitude);


		ArrayList<int[]> opts = interpreter.getLineOptions(program, 0, nVars, nArrays);

		int nEvaluated = 0;
		int nPasses = 0;
		double t = System.currentTimeMillis();
		while(true) {
			try {
				for (int i=0;i<program.length;i++) {
					//					for (int[] str:opts) {
					//						for (int q:str) {
					//							System.out.print(q + " ");
					//						}
					//						System.out.println();
					//					}
					int q = Utils.random.nextInt(opts.size());
					program[i] = opts.get(q);
				}
				int[] input = p.randomInput(8);
				int param = Utils.random.nextInt(4) + 1;
				int[] targetOutput = p.run(input.clone(), param);
				int[] out = interpreter.process(program, nArrays, nVars, input.clone(), param);

				boolean passed = true;
				for (int i=0;i<out.length;i++) {
					if (targetOutput[i] != out[i]) {
						passed = false;
						break;
					}
				}
				if (passed) {//Do some deeper checks, just to be sure
					outer:
						for (int k=0;k<10;k++) {
							input = p.randomInput(8);
							param = Utils.random.nextInt(4) + 1;
							targetOutput = p.run(input.clone(), param);
							out = interpreter.process(program, nArrays, nVars, input.clone(), param);
							for (int i=0;i<out.length;i++) {
								if (targetOutput[i] != out[i]) {
									passed = false;
									break outer;
								}
							}
						}
				}
				if (passed) {
					nPasses += 1;

					int nL = -1;
					for (int[] line:program) {
						nL += 1;
						System.out.print("Line: " + nL + " : ");
						for (int i:line) {
							System.out.print(i + " ");
						}
						System.out.println();
					}
				}
				nEvaluated += 1;
				if (nEvaluated % 1000000 == 0) {
					t = System.currentTimeMillis() - t;
					System.out.println("nEvaluated: " + Utils.commas(nEvaluated) + " Passes: " + nPasses
							+ " time(ms) " + t);
					t = System.currentTimeMillis();
				}
				//				for (int i=0;i<out.length;i++) {
				//					System.out.print(out[i] + " ");
				//				}
				//				System.out.println();
			}catch(Exception e) {}
		}
	}


	public static void testProblemDefintions() {
		Problem p = new P_Primality();
		int param = Utils.random.nextInt(4) + 1;
		int[] in = p.randomInput(8);
		int[] out = p.run(in, param);
		int[] outSimple = p.asSimple(in, param);


		System.out.print("In: ");
		for (int i:in) {
			System.out.print(i + " ");
		}
		System.out.print(" // " + param);
		System.out.println();
		System.out.print("Out: ");
		for (int i:out) {
			System.out.print(i + " ");
		}
		System.out.println();
		System.out.print("OutSimple: ");
		for (int i:outSimple) {
			System.out.print(i + " ");
		}
		System.out.println();
		for (int i=0;i<out.length;i++) {
			if (out[i] != outSimple[i]) {throw new Error();}
		}
	}

	public static ArrayList<int[]> getProgramSort(){
		ArrayList<int[]> progLines = new ArrayList<int[]>();
		progLines.add(new int[] {Gridlang.OP_VAR_TO_LITERAL,4,1,0,0});//Set var_4 to 1
		progLines.add(new int[] {Gridlang.OP_SUBTRACT,5,0,4,0});//Set var_5 to length - 1
		progLines.add(new int[] {Gridlang.OP_LOOP,2,0,0,0});//for x in range(length) LoopA
		progLines.add(new int[] {Gridlang.OP_LOOP,3,5,0,0});//for y in range(var_5)  LoopB
		progLines.add(new int[] {Gridlang.OP_ADD,6,3,4,0});//Set var_6 to var_3 + 1
		progLines.add(new int[] {Gridlang.OP_ARRAY_TO_VAR_ASSIGN,0,3,7,0});//Set var_7 to input[var_3]
		progLines.add(new int[] {Gridlang.OP_ARRAY_TO_VAR_ASSIGN,0,6,8,0});//Set var_8 to input[var_6]
		progLines.add(new int[] {Gridlang.OP_SUBTRACT,7,8,7,0});//Take difference to act as condition
		progLines.add(new int[] {Gridlang.OP_COND,7,0,0,0});//Cond on var_7
		progLines.add(new int[] {Gridlang.OP_ARRAY_TO_VAR_ASSIGN,0,3,9,0});//Set var_9 to input[var_3]
		progLines.add(new int[] {Gridlang.OP_ARRAY_TO_ARRAY_ASSIGN,0,3,0,6});
		progLines.add(new int[] {Gridlang.OP_VAR_TO_ARRAY_ASSIGN,0,6,9,0});
		progLines.add(new int[] {Gridlang.OP_ENDBLOCK,0,0,0,0});//end cond
		progLines.add(new int[] {Gridlang.OP_ENDBLOCK,0,0,0,0});//end loopB
		progLines.add(new int[] {Gridlang.OP_ENDBLOCK,0,0,0,0});//end loopA
		progLines.add(new int[] {Gridlang.OP_LOOP,2,0,0,0});//Loop to copy input to output
		progLines.add(new int[] {Gridlang.OP_ARRAY_TO_ARRAY_ASSIGN,1,2,0,2});//Set out[var_2] to in[var_2]
		progLines.add(new int[] {Gridlang.OP_ENDBLOCK,0,0,0,0});//end
		progLines.add(new int[] {Gridlang.OP_END,0,0,0,0});
		return progLines;
	}

	public static ArrayList<int[]> getProgram_AddOne(){
		ArrayList<int[]> progLines = new ArrayList<int[]>();
		progLines.add(new int[] {Gridlang.OP_VAR_TO_LITERAL,3,1,0,0});//Set var_3 to 1
		progLines.add(new int[] {Gridlang.OP_LOOP,2,0,0,0});//for x in range(length)
		progLines.add(new int[] {Gridlang.OP_ARRAY_TO_VAR_ASSIGN,0,2,4,0});//Set var_4 to input[var_2]
		progLines.add(new int[] {Gridlang.OP_ADD,4,3,4,0});//Set var_4 to var_4 + 1
		progLines.add(new int[] {Gridlang.OP_VAR_TO_ARRAY_ASSIGN,1,2,4,0});//Set out[var_2] to var_4
		progLines.add(new int[] {Gridlang.OP_ENDBLOCK,0,0,0,0});//end
		return progLines;
	}

	public static void testAllGridlangs(){
		for (Problem3 p:getP3Set()){
			testGridlang(p);
		}
	}
	public static void testGridlang(Problem3 problem) {
		int param = Utils.random.nextInt(8);
		int[] in = new int[8];
		for (int i=0;i<in.length;i++) {
			in[i] = Utils.random.nextInt(16) - 8;
		}

		int nArrays = 0;
		int nVars = 4;
		int nLines = 9;
		int[][] program = problem.getGridlang(nLines);

		Gridlang interpreter = new Gridlang();
		int[] targetOut =problem.process(in, param); 
		int[] out = interpreter.process(program, nArrays, nVars, in.clone(), param);


		System.out.print("Input : ");
		for (int i:in) {
			System.out.print(i + " ");
		}
		System.out.println( " || " + param);
		System.out.print("Output: ");
		for (int i:out) {
			System.out.print(i + " ");
		}
		System.out.println();
		System.out.print("Target: ");
		for (int i:targetOut) {
			System.out.print(i + " ");
		}
		System.out.println();
		String failureString = "Mismatches: ";
		boolean anyMismatch = false;
		for (int i=0;i<out.length;i++) {
			if (out[i] != targetOut[i]) {
				failureString += i + " ";
				anyMismatch = true;
			}
		}
		if (anyMismatch) {
			System.out.println(failureString);
		}
	}

	public static void testGridlang() {
		Problem p = new P_Sort();
		int param = Utils.random.nextInt(4) + 1;
		int[] in = p.randomInput(8);

		ArrayList<int[]> progLines = getProgram_AddOne();
		int[][] program = progLines.toArray(new int[progLines.size()][]);

		Gridlang interpreter = new Gridlang();
		int[] out = interpreter.process(program, 0, 15, in.clone(), param);
		//
		System.out.print("In: ");
		for (int i:in) {
			System.out.print(i + " ");
		}
		System.out.println();
		System.out.print("Out: ");
		for (int i:out) {
			System.out.print(i + " ");
		}
		System.out.println();

		//		long time = System.currentTimeMillis();
		//		
		//		System.out.println("Main Program:");
		//		eval(program,0,15,p);
		//		System.out.println("Mutants:");
		//
		//		for (int y = 0;y<program.length;y++) {
		//			for (int x = 0;x<program[0].length;x++) {
		//				for (int parity=0;parity<2;parity++) {
		////					System.out.println("Attempt: " + x + " " + y + " " + parity + " " + (1 + (-2*parity)));
		//					int[][] mutant = clone(program);
		//					mutant[y][x] += 1 + (-2*parity);
		//					try {
		//						eval(mutant,0,15,p);
		//					}catch(Error e) {
		////						e.printStackTrace();
		//						System.out.println("Failure err");
		//					}catch(Exception e) {
		////						e.printStackTrace();
		//						System.out.println("Failure ex");
		//					}
		//				}
		//			}
		//		}
		//		
		//		time = System.currentTimeMillis() - time;
		//		System.out.println("Time taken: " + time + " nMutants: " + (program.length*program[0].length*2));
		////				eval(program,0,15,p);
	}

	public static int[][] clone(int[][] in) {
		int[][] out = new int[in.length][in[0].length];
		for (int i=0;i<out.length;i++) {
			for (int x=0;x<out[0].length;x++) {
				out[i][x] = in[i][x];
			}
		}
		return out;
	}

	public static void eval(int[][] program,int nArrays,int nVars,Problem p) {
		double accuracy = 0;
		double baselineAccuracy = 0;
		int nTrials = 100;
		int nItems = 0;
		Gridlang interpreter = new Gridlang();

		for (int t=0;t<nTrials;t++) {
			int[] input = p.randomInput(8);
			int param = Utils.random.nextInt(4) + 1;
			int[] targetOutput = p.run(input.clone(), param);
			int[] out = interpreter.process(program, nArrays, nVars, input.clone(), param);

			for (int i=0;i<out.length;i++) {
				if (out[i] == targetOutput[i]) {accuracy += 1;}
				if (targetOutput[i] == input[i]) {baselineAccuracy += 1;}
				nItems += 1;
			}

			//			System.out.print("Input: ");
			//			for (int i=0;i<8;i++) {
			//				System.out.print(input[i] + " ");
			//			}
			//			System.out.println();
			//			System.out.print("Output: ");
			//			for (int i=0;i<8;i++) {
			//				System.out.print(out[i] + " ");
			//			}
			//			System.out.println();
			//			System.out.print("Target Output: ");
			//			for (int i=0;i<8;i++) {
			//				System.out.print(targetOutput[i] + " ");
			//			}
			//			System.out.println();
		}


		baselineAccuracy/= nItems;
		accuracy /= nItems;
		int pAcc = (int)(100*accuracy);
		int pBase = (int)(100*baselineAccuracy);
		System.out.println("Accuracy: " + pAcc + " % baseline: " + pBase +  " % gain: " + (accuracy-baselineAccuracy) + " (nItems: " + nItems + ")");
	}

	public static void testProblemDef() {
		Problem p = new P_ShiftMultiples();

		int param = 3;

		int[] in = p.randomInput(8);
		System.out.print("Input: " );
		for (int i:in) {System.out.print(i + " ");}
		System.out.println();
		int[] out = p.run(in,param);
		System.out.print("Output: " );
		for (int i:out) {System.out.print(i + " ");}
		System.out.println();

		in = new int[8];
		for (int i=0;i<in.length;i++) {in[i] = i;}
		System.out.print("Input: " );
		for (int i:in) {System.out.print(i + " ");}
		System.out.println();
		out = p.run(in,param);
		System.out.print("Output: " );
		for (int i:out) {System.out.print(i + " ");}
		System.out.println();
	}
}
