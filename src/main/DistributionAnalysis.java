package main;

import humanUsefulProblems.Problem3;

import java.util.ArrayList;
import java.util.Collections;

import autoRefactor.ARExploreByMutation;
import autoRefactor.ARGenerator;
import autoRefactor.ARSearch;

public class DistributionAnalysis {

	public static ArrayList<int[][]> toProgramSet(ArrayList<Problem3> p3set,int nLines){
		ArrayList<Problem3> set = MainClass.getP3Set();
		ArrayList<int[][]> reply = new ArrayList<int[][]>();
		for (Problem3 program:set){
			reply.add(program.getGridlang(nLines));
		}
		return reply;
	}

	public static int[] operatorCodes;
	public static double[] humanUsefulDistribution;
	public static double[] humanUsefulFlowDistribution;

	public static double[] getDistribution(int[][] program){
		double[] avrgs = new double[operatorCodes.length];
		double[] counts = new double[operatorCodes.length];
		double total = 0;

		for (int[] line:program){
			if (line[0] == 0){continue;}
			if (line[0] == -4){continue;}
			if (line[0] == -5){continue;}

			int index = -1;
			for (int i=0;i<operatorCodes.length;i++){
				if (operatorCodes[i] == line[0]){
					index = i;
				}
			}
			if (index == -1){
				System.out.println(line[0]);
				for (int i=0;i<operatorCodes.length;i++){
					System.out.println(i + " " + operatorCodes[i]);
				}
				throw new Error();
			}
			counts[index] += 1;
			total += 1;
		}

		if (total != 0){
			for (int i=0;i<counts.length;i++){
				avrgs[i] = counts[i]/total;
			}
		}

		return avrgs;
	}

	public static double getDistributionScore(int[][] program){
		if (operatorCodes == null){
			calculateAvrgDistribution_andSaveAsTarget(toProgramSet(MainClass.getP3Set(),program.length));
		}

		double[] avrgs = new double[operatorCodes.length];
		double[] flowAvrgs = new double[operatorCodes.length];
		double[] counts = new double[operatorCodes.length];
		double[] flowCounts = new double[operatorCodes.length];
		double total = 0;
		double flowTotal = 0;

		for (int[] line:program){
			if (line[0] == 0){continue;}
			if (line[0] == -4){continue;}
			if (line[0] == -5){continue;}

			int index = -1;
			for (int i=0;i<operatorCodes.length;i++){
				if (operatorCodes[i] == line[0]){
					index = i;
				}
			}
			if (index == -1){
				System.out.println(line[0]);
				for (int i=0;i<operatorCodes.length;i++){
					System.out.println(i + " " + operatorCodes[i]);
				}
				throw new Error();
			}
			counts[index] += 1;
			total += 1;

			if (line[0] < 0){
				flowCounts[index] += 1;
				flowTotal += 1;
			}
		}

		if (total != 0){
			for (int i=0;i<counts.length;i++){
				avrgs[i] = counts[i]/total;
			}
		}

		if (flowTotal != 0){
			for (int i=0;i<flowCounts.length;i++){
				flowAvrgs[i] = flowCounts[i]/flowTotal;
			}
		}
		//Euclidean distance between program's distribution and human useful one
		//Note that they cannot ever match, as human useful has too many operators touched for the line count to allow
		double distance = 0;
		for (int i=0;i<avrgs.length;i++){
			double d = avrgs[i] - humanUsefulDistribution[i];
			d *= d;
			distance += d;
		}
		distance = Math.sqrt(distance);

		return 1/distance;
	}

	public static double[] calculateAvrgDistribution(ArrayList<int[][]> programSet){
		int[][] operators = Gridlang.allOperators;

		new Gridlang();

		int c = 0;
		for (int[] optionEncoding:operators){
			if (optionEncoding == null){continue;}
			c += 1;
		}
		operatorCodes = new int[c];
		c = 0;
		for (int[] optionEncoding:operators){
			if (optionEncoding == null){continue;}
			operatorCodes[c] = optionEncoding[0];
			c += 1;
		}

		double[] avrgs = new double[c];
		double[] counts = new double[c];
		double total = 0;

		for (int[][] program:programSet){
			for (int[] line:program){
				if (line[0] == 0){continue;}
				if (line[0] == -4){continue;}
				if (line[0] == -5){continue;}

				int index = -1;
				for (int i=0;i<operatorCodes.length;i++){
					if (operatorCodes[i] == line[0]){
						index = i;
					}
				}
				counts[index] += 1;
				total += 1;
			}
		}

		if (total != 0){
			for (int i=0;i<counts.length;i++){
				avrgs[i] = counts[i]/total;
			}
		}

		for (int i=0;i<counts.length;i++){
			System.out.println(operatorCodes[i] + " " + avrgs[i]);
		}
		return avrgs;
	}

	public static void calculateAvrgDistribution_andSaveAsTarget(ArrayList<int[][]> programSet){
		int[][] operators = Gridlang.allOperators;

		new Gridlang();

		int c = 0;
		for (int[] optionEncoding:operators){
			if (optionEncoding == null){continue;}
			c += 1;
		}
		operatorCodes = new int[c];
		c = 0;
		for (int[] optionEncoding:operators){
			if (optionEncoding == null){continue;}
			operatorCodes[c] = optionEncoding[0];
			c += 1;
		}

		double[] avrgs = new double[c];
		double[] counts = new double[c];
		double total = 0;
		double[] flowAvrgs = new double[c];
		double[] flowCounts = new double[c];
		double flowTotal = 0;

		if (ARGenerator.use_flat_operator_weightings) {
			for (int i=0;i<operatorCodes.length;i++) {
				int code = operatorCodes[i];
				if (code  == Gridlang.OP_NOOP 
						|| code == Gridlang.OP_ELSEBLOCK 
						|| code == Gridlang.OP_ENDBLOCK) {
					continue;
				}
				
				if (code < 0) {
					counts[i] = 2;
					flowTotal += 1;
					flowCounts[i] = 2;
				}else {
					counts[i] = 1;
				}
				total += 1;
			}
		}else {
			for (int[][] program:programSet){
				for (int[] line:program){
					if (line[0] == 0){continue;}
					if (line[0] == -4){continue;}
					if (line[0] == -5){continue;}

					int index = -1;
					for (int i=0;i<operatorCodes.length;i++){
						if (operatorCodes[i] == line[0]){
							index = i;
						}
					}
					counts[index] += 1;
					total += 1;

					if (line[0] < 0){
						flowCounts[index] += 1;
						flowTotal += 1;
					}
				}
			}
		}

		if (total != 0){
			for (int i=0;i<counts.length;i++){
				avrgs[i] = counts[i]/total;
			}
		}
		if (flowTotal != 0){
			for (int i=0;i<flowCounts.length;i++){
				flowAvrgs[i] = flowCounts[i]/flowTotal;
			}
		}

		for (int i=0;i<counts.length;i++){
			System.out.println(operatorCodes[i] + " " + avrgs[i]);
		}
		System.out.println("Flow only:");
		for (int i=0;i<counts.length;i++){
			System.out.println(operatorCodes[i] + " " + flowAvrgs[i]);
		}

		humanUsefulDistribution = avrgs; 
		humanUsefulFlowDistribution = flowAvrgs; 
	}
}
