package main;

import java.util.ArrayList;

public class Gridlang {
	public static int maxSteps = 1024;
	public static final int OP_COND_EQUALS = -8;
//	public static final int OP_NOT_COND = -7;
	public static final int OP_END = -6;
	public static final int OP_ELSEBLOCK = -5;
	public static final int OP_ENDBLOCK = -4;
	public static final int OP_WHILE = -3;
	public static final int OP_LOOP = -2;
	public static final int OP_COND = -1;
	public static final int OP_NOOP = 0;
	
	public static final int[][] allOperators = new int[100][];
	static final int LIT = 1;
	static final int AVAR_READ = 2;
	static final int AVAR_WRITE = 3;
	static final int VAR_READ = 4;
	static final int VAR_WRITE = 5;
	
	/**
	 * Param A is always the array
	 * Param B is the index
	 * Param C is either the other array or the other variable
	 * Param D is the other array's index
	 */
	public static final int OP_ARRAY_TO_ARRAY_ASSIGN = 50;
	public static final int OP_ARRAY_TO_ARRAY_ASSIGN_ADD = 51;
	public static final int OP_VAR_TO_ARRAY_ASSIGN = 52;
	public static final int OP_ARRAY_TO_VAR_ASSIGN = 53;
	public static final int OP_ARRAY_TO_ARRAY_COPY = 54;
	public static final int OP_VAR_TO_ARRAY_ADD = 55;

	public static final int OP_VAR_TO_LITERAL = 1;
	public static final int OP_ADD = 2;
	public static final int OP_SUBTRACT = 3;
	public static final int OP_MULTIPLY = 4;
	public static final int OP_DIVIDE = 5;
	public static final int OP_MODULO = 6;
	public static final int OP_INCREMENT = 7;
	
	public static final int OP = 0;
	public static final int PARAM_A = 1;
	public static final int PARAM_B = 2;
	public static final int PARAM_C = 3;
	public static final int PARAM_D = 4;
	

	public static int builtinArrays = 2;
	public static int builtinVars = 2;
	public static int finalArrays = 1;
	public static int finalVars = 1;
	
	public Gridlang(){
		allOperators[0] = new int[] {OP_ARRAY_TO_ARRAY_ASSIGN,		AVAR_WRITE,VAR_READ,AVAR_READ,VAR_READ};
//		allOperators[1] = new int[] {OP_ARRAY_TO_ARRAY_ASSIGN_ADD,	AVAR_WRITE,VAR_READ,AVAR_READ,VAR_READ};
		allOperators[2] = new int[] {OP_VAR_TO_ARRAY_ASSIGN,		AVAR_WRITE,VAR_READ,VAR_READ,0};
		allOperators[3] = new int[] {OP_ARRAY_TO_VAR_ASSIGN,		AVAR_READ,VAR_READ,VAR_WRITE,0};
//		allOperators[4] = new int[] {OP_ARRAY_TO_ARRAY_COPY,		AVAR_WRITE,AVAR_READ,0,0};
		allOperators[5] = new int[] {OP_VAR_TO_ARRAY_ADD,			AVAR_WRITE,VAR_READ,VAR_READ,0};
		
		allOperators[6] = new int[] {OP_VAR_TO_LITERAL,				VAR_WRITE,LIT,0,0};
		allOperators[7] = new int[] {OP_ADD,						VAR_WRITE,VAR_READ,VAR_READ,0};
		allOperators[8] = new int[] {OP_SUBTRACT,					VAR_WRITE,VAR_READ,VAR_READ,0};
		allOperators[9] = new int[] {OP_MULTIPLY,					VAR_WRITE,VAR_READ,VAR_READ,0};
		allOperators[10] = new int[] {OP_DIVIDE,					VAR_WRITE,VAR_READ,VAR_READ,0};
		allOperators[11] = new int[] {OP_MODULO,					VAR_WRITE,VAR_READ,VAR_READ,0};
		allOperators[12] = new int[] {OP_INCREMENT,					VAR_WRITE,0,0,0};

		allOperators[13] = new int[] {OP_LOOP,						VAR_WRITE,VAR_READ,0,0};
		allOperators[14] = new int[] {OP_COND,						VAR_READ,0,0,0};
		allOperators[15] = new int[] {OP_NOOP,						0,0,0,0};
//		allOperators[16] = new int[] {OP_NOT_COND,					VAR_READ,0,0,0};
		allOperators[17] = new int[] {OP_COND_EQUALS,				VAR_READ,VAR_READ,0,0};
//		allOperators[18] = new int[] {OP_WHILE,						VAR_WRITE,0,0,0};
	}

	public int[] process(int[][] program,int nArrayVars,int nVars,int[] input,int param) {
		return process(program,nArrayVars,nVars,input,param,null);
	}
	public int[] process(int[][] program,int nArrayVars,int nVars,int[] input,int param,Monitor monitor) {
		return process(program,nArrayVars,nVars,new int[][]{input},new int[]{param},monitor);
	}
	public int[] process(int[][] program,int nArrayVars,int nVars,int[][] inputArrays,int[] inputParams,Monitor monitor){
		int[][] arrayVars = new int[nArrayVars+builtinArrays][inputArrays[0].length];//+2: input, output
		for (int i=0;i<inputArrays.length;i++){
			arrayVars[i] = inputArrays[i].clone();
		}
		int[] vars = new int[nVars+builtinVars ];//+2: inputLength,param
		vars[0] = inputArrays[0].length;
		for (int i=0;i<inputParams.length;i++){
			vars[i+1] = inputParams[i];
		}
		
		int head = 0;
		int step = -1;
		while(step < maxSteps) {
			step += 1;
			if (monitor != null){monitor.lineUseCount[head] += 1;}
			if (program[head][OP]  == OP_ENDBLOCK) {
				int depth = 1;
//				System.out.println("Scan backwards from " + head + " at target: " + targetScope);
//				//We need to check if we're allowed to leave the loop
				for (int i=head-1;i>=0;i--) {
//					System.out.println("Line: " + i + " scope " + program[i][SCOPE]);
					//Scan backwards till you hit the block start
					if (program[i][OP] == OP_LOOP) {
						depth -= 1;
					}
					else if (program[i][OP] == OP_COND) {
						depth -= 1;
					}
//					else if (program[i][OP] == OP_NOT_COND) {
//						depth -= 1;
//					}
					else if (program[i][OP] == OP_COND_EQUALS) {
						depth -= 1;
					}
					else if (program[i][OP] == OP_ENDBLOCK) {
						depth += 1;
					}
						
					if (depth == 0) {
						if (program[i][OP] == OP_LOOP) {
							//We're at the loop declaration
							vars[program[i][PARAM_A]] += 1;
							if (vars[program[i][PARAM_A]] >= vars[program[i][PARAM_B]]) {
								//We're exiting the loop
							}else {
								//We're continuing the loop
								head = i + 1;//Send head back to start of loop
								break;
							}
						}
						else if (program[i][OP] == OP_WHILE) {
							if (monitor != null){monitor.conditionAttempts[i] += 1;}
							//We're at the loop declaration
							if (vars[program[i][PARAM_A]] <= 0) {
								//We're exiting the loop
							}else {
								if (monitor != null){monitor.conditionSuccesses[i] += 1;}
								//We're continuing the loop
								head = i + 1;//Send head back to start of loop
								break;
							}
						}else if (program[i][OP] == OP_COND) {
							//Nothing relevant. We're just leaving a conditional block
						}
//						else if (program[i][OP] == OP_NOT_COND) {
//							//Nothing relevant. We're just leaving a conditional block
//						}
						else if (program[i][OP] == OP_COND_EQUALS) {
							//Nothing relevant. We're just leaving a conditional block
						}else{
							throw new Error("Malformed end to block. Line " + head);
						}
						break;
					}
				}
			}
			
			//Termination condition reached
			if (program[head][OP] == OP_END) {
				break;
			}
			
			//Now actually process some ops
			if (program[head][OP] == OP_LOOP) {
				//First up, set iterator to 0
				vars[program[head][PARAM_A]] = 0;
				
				//For a loop, param A is the iterator variable, param B is the range
				//If you're being asked to iterate across zero or fewer elements, skip block
				if (vars[program[head][PARAM_B]] <= 0) {
					//Skip entire block
					for (;head<program.length;head++) {
						if (program[head][OP] == OP_ENDBLOCK) {
							break;
						}
					}
				}
			}
			else if (program[head][OP] == OP_WHILE) {
				//Skip block if your condition has already failed
				if (monitor != null){monitor.conditionAttempts[head] += 1;}
				if (vars[program[head][PARAM_A]] <= 0) {
					//Skip entire block
					for (;head<program.length;head++) {
						if (program[head][OP] == OP_ENDBLOCK) {
							break;
						}
					}
				}else{
					if (monitor != null){monitor.conditionSuccesses[head] += 1;}
				}
				//Otherwise, continue normally
			}
			else if (program[head][OP] == OP_COND) {
				if (monitor != null){monitor.conditionAttempts[head] += 1;}
				if (vars[program[head][PARAM_A]] <= 0) {
					//Skip entire block
					for (;head<program.length;head++) {
						if (program[head][OP] == OP_ENDBLOCK) {
							break;
						}
						if (program[head][OP] == OP_ELSEBLOCK) {
							break;
						}
					}
				}else{
					if (monitor != null){monitor.conditionSuccesses[head] += 1;}
				}
				//Otherwise, continue normally
			}
//			else if (program[head][OP] == OP_NOT_COND) {
//				if (monitor != null){monitor.conditionAttempts[head] += 1;}
//				if (vars[program[head][PARAM_A]] > 0) {
//					//Skip entire block
//					for (;head<program.length;head++) {
//						if (program[head][OP] == OP_ENDBLOCK) {
//							break;
//						}
//						if (program[head][OP] == OP_ELSEBLOCK) {
//							break;
//						}
//					}
//				}else{
//					if (monitor != null){monitor.conditionSuccesses[head] += 1;}
//				}
//				//Otherwise, continue normally
//			}
			else if (program[head][OP] == OP_COND_EQUALS) {
				if (monitor != null){monitor.conditionAttempts[head] += 1;}
				if (vars[program[head][PARAM_A]] != vars[program[head][PARAM_B]]) {
					//Skip entire block
					for (;head<program.length;head++) {
						if (program[head][OP] == OP_ENDBLOCK) {
							break;
						}
						if (program[head][OP] == OP_ELSEBLOCK) {
							break;
						}
					}
				}else{
					if (monitor != null){monitor.conditionSuccesses[head] += 1;}
				}
				//Otherwise, continue normally
			}
			else if (program[head][OP] == OP_ELSEBLOCK) {//Arrived at else block through head increment, skip block
				//Skip entire block
				for (;head<program.length;head++) {
					if (program[head][OP] == OP_ENDBLOCK) {
						break;
					}
				}
			}
			else if (program[head][OP] < 50) {
				varOP(monitor,step,program[head],arrayVars,vars);
			}
			else if (program[head][OP] >= 50 && program[head][OP] < 100) {
				arrayOp(monitor,step,program[head],arrayVars,vars);
			}
			
			head += 1;
			if (head >= program.length) {
				//Off end of program tape
				break;
			}
		}
		if (monitor != null) {monitor.stepsUsed += step;}
		return arrayVars[arrayVars.length-1];
	}

	private void varOP(Monitor m,int step,int[] instructions, int[][] arrayVars, int[] vars) {
		int paramA = instructions[PARAM_A];
		int paramB = instructions[PARAM_B];
		int paramC = instructions[PARAM_C];
//		int paramD = instructions[PARAM_D];
		if (instructions[OP] == OP_VAR_TO_LITERAL) {
			vars[paramA] = paramB;
			
			if (m != null){
				m.varWriteHistories[step][paramA] += 1;
			}
		}
		else if (instructions[OP] == OP_ADD) {
			vars[paramA] = vars[paramB] + vars[paramC];

			if (m != null){
				m.varWriteHistories[step][paramA] += 1;
				m.varAccessHistories[step][paramB] += 1;
				m.varAccessHistories[step][paramC] += 1;
			}
		}
		else if (instructions[OP] == OP_SUBTRACT) {
			vars[paramA] = vars[paramB] - vars[paramC];

			if (m != null){
				m.varWriteHistories[step][paramA] += 1;
				m.varAccessHistories[step][paramB] += 1;
				m.varAccessHistories[step][paramC] += 1;
			}
		}
		else if (instructions[OP] == OP_MULTIPLY) {
			vars[paramA] = vars[paramB] * vars[paramC];

			if (m != null){
				m.varWriteHistories[step][paramA] += 1;
				m.varAccessHistories[step][paramB] += 1;
				m.varAccessHistories[step][paramC] += 1;
			}
		}
		else if (instructions[OP] == OP_DIVIDE) {
			if (vars[paramC] != 0) {
				vars[paramA] = vars[paramB] / vars[paramC];
			}else {
				vars[paramA] = 0;
			}

			if (m != null){
				m.varWriteHistories[step][paramA] += 1;
				m.varAccessHistories[step][paramB] += 1;
				m.varAccessHistories[step][paramC] += 1;
			}
		}
		else if (instructions[OP] == OP_MODULO) {
			if (vars[paramC] != 0) {
				vars[paramA] = vars[paramB] % vars[paramC];
			}else {
				vars[paramA] = 0;
			}

			if (m != null){
				m.varWriteHistories[step][paramA] += 1;
				m.varAccessHistories[step][paramB] += 1;
				m.varAccessHistories[step][paramC] += 1;
			}
		}
		else if (instructions[OP] == OP_INCREMENT) {
			vars[paramA]  += 1;

			if (m != null){
				m.varWriteHistories[step][paramA] += 1;
			}
		}
	}

	private void arrayOp(Monitor monitor,int step,int[] instructions, int[][] arrayVars, int[] vars) {
		int paramA = instructions[PARAM_A];
		int paramB = instructions[PARAM_B];
		int paramC = instructions[PARAM_C];
		int paramD = instructions[PARAM_D];
//		int indexA = vars[paramB];
//		int maxLen = arrayVars[paramA].length;
//		indexA = indexA % maxLen;
//		if (indexA < 0){
//			indexA = maxLen + indexA;
//		}
//		int indexB = vars[paramB];
//		maxLen = arrayVars[paramB].length;
//		indexB = indexB % maxLen;
//		if (indexB < 0){
//			indexB = maxLen + indexB;
//		}
		if (instructions[OP] == OP_ARRAY_TO_ARRAY_ASSIGN) {
			arrayVars[paramA][vars[paramB]] = arrayVars[paramC][vars[paramD]];
			//Note this in the monitor, if any exist, to evaluate uniformity of reading/writing
			if (monitor != null){
				//READ
				if (monitor.arrayAccessCounts[paramC] == null){
					monitor.arrayAccessCounts[paramC] = new int[arrayVars[paramC].length];
				}
				if (monitor.arrayAccessCounts[paramC].length != arrayVars[paramC].length){
					throw new Error("" + monitor.arrayAccessCounts[paramC].length + " " +  arrayVars[paramC].length);
				}
				monitor.arrayAccessCounts[paramC][vars[paramD]] += 1;
				
				if (monitor.arrayAccessHistories[step][paramC] == null){
					monitor.arrayAccessHistories[step][paramC] = new int[arrayVars[paramC].length];
				}
				if (monitor.arrayAccessHistories[step][paramC].length != arrayVars[paramC].length){
					throw new Error("" + monitor.arrayAccessHistories[step][paramC].length + " " +  arrayVars[paramC].length);
				}
				monitor.arrayAccessHistories[step][paramC][vars[paramD]] += 1;
			}
			if (monitor != null){
				//WRITE
				if (monitor.arrayWriteCounts[paramA] == null){
					monitor.arrayWriteCounts[paramA] = new int[arrayVars[paramA].length];
				}
				if (monitor.arrayWriteCounts[paramA].length != arrayVars[paramA].length){
					throw new Error("" + monitor.arrayWriteCounts[paramA].length + " " +  arrayVars[paramA].length);
				}
				monitor.arrayWriteCounts[paramA][vars[paramB]] += 1;
				
				if (monitor.arrayWriteHistories[step][paramA] == null){
					monitor.arrayWriteHistories[step][paramA] = new int[arrayVars[paramA].length];
				}
				if (monitor.arrayWriteHistories[step][paramA].length != arrayVars[paramA].length){
					throw new Error("" + monitor.arrayWriteHistories[step][paramA].length + " " +  arrayVars[paramA].length);
				}
				monitor.arrayWriteHistories[step][paramA][vars[paramB]] += 1;
			}
		}
		else if (instructions[OP] == OP_ARRAY_TO_ARRAY_ASSIGN_ADD) {
			arrayVars[paramA][vars[paramB]] += arrayVars[paramC][vars[paramD]];
			//Note this in the monitor, if any exist, to evaluate uniformity of reading/writing
			if (monitor != null){
				//READ
				if (monitor.arrayAccessCounts[paramC] == null){
					monitor.arrayAccessCounts[paramC] = new int[arrayVars[paramC].length];
				}
				if (monitor.arrayAccessCounts[paramC].length != arrayVars[paramC].length){
					throw new Error("" + monitor.arrayAccessCounts[paramC].length + " " +  arrayVars[paramC].length);
				}
				monitor.arrayAccessCounts[paramC][vars[paramD]] += 1;
				//READ
				if (monitor.arrayAccessHistories[step][paramC] == null){
					monitor.arrayAccessHistories[step][paramC] = new int[arrayVars[paramC].length];
				}
				if (monitor.arrayAccessHistories[step][paramC].length != arrayVars[paramC].length){
					throw new Error("" + monitor.arrayAccessHistories[step][paramC].length + " " +  arrayVars[paramC].length);
				}
				monitor.arrayAccessHistories[step][paramC][vars[paramD]] += 1;
			}
			if (monitor != null){
				//WRITE
				if (monitor.arrayWriteCounts[paramA] == null){
					monitor.arrayWriteCounts[paramA] = new int[arrayVars[paramA].length];
				}
				if (monitor.arrayWriteCounts[paramA].length != arrayVars[paramA].length){
					throw new Error("" + monitor.arrayWriteCounts[paramA].length + " " +  arrayVars[paramA].length);
				}
				monitor.arrayWriteCounts[paramA][vars[paramB]] += 1;
				//WRITE
				if (monitor.arrayWriteHistories[step][paramA] == null){
					monitor.arrayWriteHistories[step][paramA] = new int[arrayVars[paramA].length];
				}
				if (monitor.arrayWriteHistories[step][paramA].length != arrayVars[paramA].length){
					throw new Error("" + monitor.arrayWriteHistories[step][paramA].length + " " +  arrayVars[paramA].length);
				}
				
				
				monitor.arrayWriteHistories[step][paramA][vars[paramB]] += 1;
			}
		}
		else if (instructions[OP] == OP_VAR_TO_ARRAY_ASSIGN) {
			//Note this in the monitor, if any exist, to evaluate uniformity of reading/writing
			if (monitor != null){
				//WRITE
				if (monitor.arrayWriteCounts[paramA] == null){
					monitor.arrayWriteCounts[paramA] = new int[arrayVars[paramA].length];
				}
				if (monitor.arrayWriteCounts[paramA].length != arrayVars[paramA].length){
					throw new Error("" + monitor.arrayWriteCounts[paramA].length + " " +  arrayVars[paramA].length);
				}
				monitor.arrayWriteCounts[paramA][vars[paramB]] += 1;
				//WRITE
				if (monitor.arrayWriteHistories[step][paramA] == null){
					monitor.arrayWriteHistories[step][paramA] = new int[arrayVars[paramA].length];
				}
				if (monitor.arrayWriteHistories[step][paramA].length != arrayVars[paramA].length){
					throw new Error("" + monitor.arrayWriteHistories[step][paramA].length + " " +  arrayVars[paramA].length);
				}
				monitor.arrayWriteHistories[step][paramA][vars[paramB]] += 1;
				
				monitor.varAccessHistories[step][paramB] += 1;
			}

			//Do the operation after, in case it's altering its own variable
			arrayVars[paramA][vars[paramB]] = vars[paramC];
		}
		else if (instructions[OP] == OP_VAR_TO_ARRAY_ADD) {
			//Note this in the monitor, if any exist, to evaluate uniformity of reading/writing
			if (monitor != null){
				//WRITE
				if (monitor.arrayWriteCounts[paramA] == null){
					monitor.arrayWriteCounts[paramA] = new int[arrayVars[paramA].length];
				}
				if (monitor.arrayWriteCounts[paramA].length != arrayVars[paramA].length){
					throw new Error("" + monitor.arrayWriteCounts[paramA].length + " " +  arrayVars[paramA].length);
				}
				monitor.arrayWriteCounts[paramA][vars[paramB]] += 1;
				//WRITE
				if (monitor.arrayWriteHistories[step][paramA] == null){
					monitor.arrayWriteHistories[step][paramA] = new int[arrayVars[paramA].length];
				}
				if (monitor.arrayWriteHistories[step][paramA].length != arrayVars[paramA].length){
					throw new Error("" + monitor.arrayWriteHistories[step][paramA].length + " " +  arrayVars[paramA].length);
				}
				monitor.arrayWriteHistories[step][paramA][vars[paramB]] += 1;
			}

			//Do the operation after, in case it's altering its own variable
			arrayVars[paramA][vars[paramB]] += vars[paramC];
		}
		else if (instructions[OP] == OP_ARRAY_TO_VAR_ASSIGN) {
			if (monitor != null){
				//READ
				if (monitor.arrayAccessCounts[paramA] == null){
					monitor.arrayAccessCounts[paramA] = new int[arrayVars[paramA].length];
				}
				if (monitor.arrayAccessCounts[paramA].length != arrayVars[paramA].length){
					throw new Error("" + monitor.arrayAccessCounts[paramC].length + " " +  arrayVars[paramA].length);
				}
				monitor.arrayAccessCounts[paramA][vars[paramB]] += 1;
				//READ
				if (monitor.arrayAccessHistories[step][paramA] == null){
					monitor.arrayAccessHistories[step][paramA] = new int[arrayVars[paramA].length];
				}
				if (monitor.arrayAccessHistories[step][paramA].length != arrayVars[paramA].length){
					throw new Error("" + monitor.arrayAccessHistories[step][paramC].length + " " +  arrayVars[paramA].length);
				}
				monitor.arrayAccessHistories[step][paramA][vars[paramB]] += 1;
				monitor.varWriteHistories[step][paramB] += 1;
//				System.out.println("Read from array " + paramA + " index " + vars[paramB] + " step " + step + " currently " + monitor.arrayAccessHistories[step][paramA][vars[paramB]]);
			}
			
			//Do the operation after, in case it's altering its own variable
			vars[paramC] = arrayVars[paramA][vars[paramB]];
		}
		else if (instructions[OP] == OP_ARRAY_TO_ARRAY_COPY) {
			arrayVars[paramA] = arrayVars[paramB].clone();
		}
	}

	public ArrayList<int[]> getRestrictedOptions(int[][] program,int line,int nVars,int nArrays){
		ArrayList<int[]> opts = this.getLineOptions(program, line, nVars, nArrays);

//		//The entire thing must live inside a loop. By law
//		if (line == 0) {
//			ArrayList<int[]> subset = new ArrayList<int[]>();
//			for (int[] op:opts) {
//				if (op[OP] == OP_LOOP && op[PARAM_A] == 2) {
//					subset.add(op);
//				}
//			}
//			opts = subset;
//		}
//		if (line == program.length-1) {
//			ArrayList<int[]> subset = new ArrayList<int[]>();
//			for (int[] op:opts) {
//				if (op[OP] == OP_ENDBLOCK) {
//					subset.add(op);
//				}
//			}
//			opts = subset;
//		}
		//-------------------------------
		
//		if (line == 0) {
//			ArrayList<int[]> subset = new ArrayList<int[]>();
//			for (int[] op:opts) {
//				if (op[OP] == OP_VAR_TO_LITERAL) {
//					subset.add(op);
//				}
//			}
//			opts = subset;
//		}
//		if (line == 1) {
//			ArrayList<int[]> subset = new ArrayList<int[]>();
//			for (int[] op:opts) {
//				if (op[OP] == OP_LOOP && op[PARAM_A] == 2) {
//					subset.add(op);
//				}
//			}
//			opts = subset;
//		}
//		if (line == 2) {
//			ArrayList<int[]> subset = new ArrayList<int[]>();
//			for (int[] op:opts) {
//				if (op[OP] == OP_ARRAY_TO_VAR_ASSIGN) {
//					subset.add(op);
//				}
//			}
//			opts = subset;
//		}
//		if (line == 3) {
//			ArrayList<int[]> subset = new ArrayList<int[]>();
//			for (int[] op:opts) {
//				if (op[OP] == OP_ADD) {
//					subset.add(op);
//				}
//			}
//			opts = subset;
//		}
//		if (line == program.length-1) {
//			ArrayList<int[]> subset = new ArrayList<int[]>();
//			for (int[] op:opts) {
//				if (op[OP] == OP_ENDBLOCK) {
//					subset.add(op);
//				}
//			}
//			opts = subset;
//		}
		
		return opts;
	}
	
	public ArrayList<int[]> getLineOptions(int[][] program,int line,int nVars,int nArrays){
		return getMaximalOptions(nVars,nArrays);
//		ArrayList<int[]> reply = new ArrayList<int[]>();
//		
//		nVars += builtinVars;
//		nArrays += builtinArrays;
//		
//		reply.add(new int[] {OP_NOOP,0,0,0,0});
//		
//		if (line >= 2) {//Can't end on the first element, nor the second (or why have a loop?)
//			reply.add(new int[] {OP_ENDBLOCK,0,0,0,0});
//		}
//		
//		if (line >= 2) {//Can't start an else without an if and another expression
//			reply.add(new int[] {OP_ELSEBLOCK,0,0,0,0});
//		}
//		
//		for (int i=0;i<allOperators.length;i++) {
//			if (allOperators[i] != null) {
//				addOptions(allOperators[i], nVars,nArrays, reply);
//			}
//		}
//		
//		return reply;
	}

	public ArrayList<int[]> getMaximalOptions(int nVars,int nArrays){
		ArrayList<int[]> reply = new ArrayList<int[]>();
		
		nVars += builtinVars;
		nArrays += builtinArrays;
		
		reply.add(new int[] {OP_NOOP,0,0,0,0});
		
		reply.add(new int[] {OP_ENDBLOCK,0,0,0,0});
	
		reply.add(new int[] {OP_ELSEBLOCK,0,0,0,0});
		
		for (int i=0;i<allOperators.length;i++) {
			if (allOperators[i] != null) {
				addOptions(allOperators[i], nVars,nArrays, reply);
			}
		}
		
		return reply;
	}
	
	public void addOptions(int[] operatorDef,int nVars,int nArrays,ArrayList<int[]> options) {
		int[][] opts = new int[5][];
		opts[0] = new int[] {operatorDef[0]};
		
		for (int i=1;i<operatorDef.length;i++) {
			if (operatorDef[i] == AVAR_READ) {
				opts[i] = new int[nArrays+builtinArrays];
				for (int j=0;j<nArrays+builtinArrays;j++) {
					opts[i][j] = j;
				}
			}
			else if (operatorDef[i] == VAR_READ) {
				opts[i] = new int[nVars];
				for (int j=0;j<nVars;j++) {
					opts[i][j] = j;
				} 
			}
			else if (operatorDef[i] == AVAR_WRITE) {
				opts[i] = new int[nArrays-finalArrays];
				for (int j=0;j<nArrays-finalArrays;j++) {
					opts[i][j] = j+finalArrays;
				}
			}
			else if (operatorDef[i] == VAR_WRITE) {//Skip final variables (arrayLen)
				opts[i] = new int[nVars-finalVars];
				for (int j=0;j<nVars-finalVars;j++) {
					opts[i][j] = j+finalVars;
				} 
			}
			else if (operatorDef[i] == LIT) {
				opts[i] = new int[] {-1,0,1};
			}
			else {
				opts[i] = new int[] {0};
			}
		}
		
		for (int v=0;v<opts[1].length;v++) {
			for (int j=0;j<opts[2].length;j++) {
				for (int k=0;k<opts[3].length;k++) {
					for (int q=0;q<opts[4].length;q++) {
						int[] option = new int[] {opts[0][0],opts[1][v],opts[2][j],opts[3][k],opts[4][q]};
						options.add(option);
					}
				}
			}
		}
		
	}
}
