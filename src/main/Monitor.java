package main;

public class Monitor {
	public double[] lineUseCount;
	public double[] conditionAttempts;
	public double[] conditionSuccesses;
	public int stepsUsed;
	public int[][] arrayAccessCounts;
	public int[][] arrayWriteCounts;
	public int[][][] arrayAccessHistories;
	public int[][][] arrayWriteHistories;
	public int[][] varAccessHistories;
	public int[][] varWriteHistories;
	
	public Monitor(int nLines,int nArrays,int nVars){
		lineUseCount = new double[nLines];
		conditionAttempts = new double[nLines];
		conditionSuccesses = new double[nLines];
		arrayAccessCounts = new int[nArrays+Gridlang.builtinArrays][];
		arrayWriteCounts = new int[nArrays+Gridlang.builtinArrays][];
		arrayAccessHistories = new int[Gridlang.maxSteps*2][nArrays+Gridlang.builtinArrays][8];
		arrayWriteHistories = new int[Gridlang.maxSteps*2][nArrays+Gridlang.builtinArrays][8];
		varAccessHistories = new int[Gridlang.maxSteps*2][nVars+Gridlang.builtinVars];
		varWriteHistories = new int[Gridlang.maxSteps*2][nVars+Gridlang.builtinVars];
	}
}
