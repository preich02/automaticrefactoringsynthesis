package main;

import java.util.ArrayList;
import java.util.Random;

public class Utils {
	public static Random random = new Random();

	public static String commas(long num) {
		String letters = "" + num;
		String reply = "";
		for (int i=0;i<letters.length();i++) {
			int left = i - letters.length();
			if (i != 0 && left % 3 == 0) {
				reply += ",";
			}
			reply += letters.charAt(i);
		}
		return reply;
	}
	public static double[] intToBinary(int toEncode,int encodingLength) {
		int original = toEncode;
		int index = 0;
		double writeValue = 1;
		double[] indexEncoding = new double[encodingLength];
		
		//Encode negative numbers
		if (toEncode < 0){
			indexEncoding[0] = 1;
			toEncode = -toEncode;
		}
		index = 1;
		
		while (toEncode > 0) {
			if (toEncode % 2 == 1) {
				toEncode -= 1;
				indexEncoding[index] += writeValue;
			}
			else {
				indexEncoding[index] += 0;
			}
			toEncode /= 2;
			index += 1;
			if (index >= indexEncoding.length) {
				throw new Error("Overflow when converting to binary. Can't fit " + original + " into " + encodingLength);
			}
		}
		return indexEncoding;
	}

	public static double[] intToBinary_Ceiling(int toEncode,int encodingLength) {
		int original = toEncode;
		int index = 0;
		double writeValue = 1;
		double[] indexEncoding = new double[encodingLength];
		
		//Encode negative numbers
		if (toEncode < 0){
			indexEncoding[0] = 1;
			toEncode = -toEncode;
		}
		index = 1;
		
		while (toEncode > 0) {
			if (toEncode % 2 == 1) {
				toEncode -= 1;
				indexEncoding[index] += writeValue;
			}
			else {
				indexEncoding[index] += 0;
			}
			toEncode /= 2;
			index += 1;
			if (index >= indexEncoding.length) {
				System.out.println("Overflow when converting to binary. Can't fit " + original + " into " + encodingLength);
				break;
			}
		}
		return indexEncoding;
	}
	
	public static double[] intToBinaryOverflowable(int toEncode,int encodingLength) {
		int index = 0;
		double writeValue = 1;
		double[] indexEncoding = new double[encodingLength];
		while (toEncode > 0) {
			if (toEncode % 2 == 1) {
				toEncode -= 1;
				indexEncoding[index] += writeValue;
			}
			else {
				indexEncoding[index] += 0;
			}
			toEncode /= 2;
			index += 1;
			//Loop back, if you somehow overflow. Start writing a different value, to at least give
			//some clue that this has occurred
			if (index >= indexEncoding.length) {
				index = 0;
				writeValue /= 2;
			}
		}
		return indexEncoding;
	}
	public static double[] intToTernary(int toEncode,int encodingLength) {
		//How to encode to ternary. I think.
		//Index = 0;
		//Is it divisible by three? If so, div by three, write 0 at [index]
		//Else: subtract 1, div by three, write remainder at [index]
		//Index += 1;
		//Continue till number is zero
		int index = 0;
		double writeValue = 1;
		double[] indexEncoding = new double[encodingLength];
		while (toEncode > 0) {
			if (toEncode % 3 == 1) {
				toEncode -= 1;
				indexEncoding[index] += writeValue;
			}
			else if (toEncode % 3 == 2) {
				toEncode -= 2;
				indexEncoding[index] -= writeValue;
			}else {
				indexEncoding[index] += 0;
			}
			toEncode /= 3;
			index += 1;
			//Loop back, if you somehow overflow. Start writing a different value, to at least give
			//some clue that this has occurred
			if (index >= indexEncoding.length) {
				index = 0;
				writeValue /= 2;
			}
		}
		return indexEncoding;
	}
	public static int roulette(double[] ds) {
		ds = ds.clone();
		double sum = 0;
		double min = ds[0];
		for (int i=0;i<ds.length;i++) {
			if (ds[i] < min) {min = ds[i];}
		}
		for (int i=0;i<ds.length;i++) {
			ds[i] -= min;
			sum += ds[i];
		}
		if (sum == 0) {return Utils.random.nextInt(ds.length);}
		double roll = Utils.random.nextDouble()*sum;
		for (int i=0;i<ds.length;i++) {
			roll -= ds[i];
			if (roll <= 0) {return i;}
		}
		return -1;
	}
	public static int roulette(double[] ds, boolean[] unselectable) {
		double sum = 0;
		double min = ds[0];
		for (int i=0;i<ds.length;i++) {
			if (unselectable[i]) {continue;}
			if (ds[i] < min) {min = ds[i];}
		}
		for (int i=0;i<ds.length;i++) {
			if (unselectable[i]) {continue;}
			ds[i] -= min;
			sum += ds[i];
		}
		if (sum == 0) {
			int c = 0;
			int choice = -1;
			for (int i=0;i<ds.length;i++) {
				if (!unselectable[i]) {
					c += 1;
					if (Utils.random.nextInt(c) == 0) {
						choice = i;
					}
				}
			}
			return choice;
		}
		double roll = Utils.random.nextDouble()*sum;
		for (int i=0;i<ds.length;i++) {
			if (unselectable[i]) {continue;}
			roll -= ds[i];
			if (roll <= 0) {return i;}
		}
		return -1;
	}
	public static double[] cat(double[] a, double[] b) {
		double[] c = new double[a.length+b.length];
		for (int i=0;i<a.length;i++) {
			c[i] = a[i];
		}
		for (int i=0;i<b.length;i++) {
			c[i+a.length] = b[i];
		}
		return c;
	}
	public static String pad(double d) {
		int i = (int)(1000*d);
		String str = "" + i;
		while (str.length() < 4) {str = " " + str;}
		return str;
	}
	public static String pad(int i) {
		String str = "" + i;
		while (str.length() < 3) {str = " " + str;}
		return str;
	}
	public static int tournament(double[] fitnesses, int size) {
		int winner = Utils.random.nextInt(fitnesses.length);
		for (int i=0;i<size;i++) {
			int q = Utils.random.nextInt(fitnesses.length);
			if (fitnesses[q] > fitnesses[i]) {
				winner = q;
			}
		}
		return winner;
	}
}
