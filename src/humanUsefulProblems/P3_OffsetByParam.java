package humanUsefulProblems;

import java.util.ArrayList;

import main.Gridlang;

public class P3_OffsetByParam extends Problem3{

	@Override
	public int[] process(int[] input, int param) {
		int[] out = new int[input.length];
		for (int i=0;i<out.length-param;i++) {
			out[i+param] = input[i];
		}
		return out;
	}

	@Override
	public int[][] getGridlang(int nLines) {
		ArrayList<int[]> lines = new ArrayList<int[]>();
		lines.add(new int[] {Gridlang.OP_SUBTRACT,4,0,1,0});//var_4 = len - param
		lines.add(new int[] {Gridlang.OP_LOOP,2,4,0,0});//iterator to 2, range(input-param)
		lines.add(new int[] {Gridlang.OP_ADD,3,2,1,0});//var_3 = var_2 + param
		lines.add(new int[] {Gridlang.OP_ARRAY_TO_ARRAY_ASSIGN,1,3,0,2});
		lines.add(new int[] {Gridlang.OP_ENDBLOCK,0,0,0,0});//Endloop
		while(lines.size() < nLines) {lines.add(new int[] {0,0,0,0,0});}
		
		int[][] program = new int[nLines][5];
		for (int i=0;i<program.length;i++) {
			for (int j=0;j<5;j++) {
				program[i][j] = lines.get(i)[j];
			}
		}
		return program;
	}

}
