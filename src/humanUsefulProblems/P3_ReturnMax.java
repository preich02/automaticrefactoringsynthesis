package humanUsefulProblems;

import java.util.ArrayList;

import main.Gridlang;

public class P3_ReturnMax extends Problem3{

	@Override
	public int[] process(int[] input, int param) {
		int[] out = new int[input.length];
		int k = 0;
		for (int i=0;i<out.length;i++) {
			if (input[i] > k) {
				k = input[i];
			}
		}
		out[0] = k;
		return out;
	}

	@Override
	public int[][] getGridlang(int nLines) {
		ArrayList<int[]> lines = new ArrayList<int[]>();
		//Var 5 is the seen max
		//If max - new_value > 0, new_value is greater than max
		//var 4 is the conditional variable, therefore
		lines.add(new int[] {Gridlang.OP_LOOP,2,0,0,0});//iterator to 2
		lines.add(new int[] {Gridlang.OP_ARRAY_TO_VAR_ASSIGN,0,2,4,0});//var_4 = in[var_2]
		lines.add(new int[] {Gridlang.OP_SUBTRACT,4,4,5,0});//var_4 = var_5 - var_4 
		lines.add(new int[] {Gridlang.OP_COND,4,0,0,0}); 
		lines.add(new int[] {Gridlang.OP_ARRAY_TO_VAR_ASSIGN,0,2,5,0});//var_5 = in[var_2]
		lines.add(new int[] {Gridlang.OP_ENDBLOCK,0,0,0,0});//Endif
		lines.add(new int[] {Gridlang.OP_ENDBLOCK,0,0,0,0});//Endloop

		lines.add(new int[] {Gridlang.OP_VAR_TO_LITERAL,4,0,0,0});//var_4 = 0 
		lines.add(new int[] {Gridlang.OP_VAR_TO_ARRAY_ASSIGN,1,4,5,0});//out[4] = var_5
		while(lines.size() < nLines) {lines.add(new int[] {0,0,0,0,0});}
		
		int[][] program = new int[nLines][5];
		for (int i=0;i<program.length;i++) {
			for (int j=0;j<5;j++) {
				program[i][j] = lines.get(i)[j];
			}
		}
		return program;
	}

}
