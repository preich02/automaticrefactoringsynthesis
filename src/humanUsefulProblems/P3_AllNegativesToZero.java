package humanUsefulProblems;

import java.util.ArrayList;

import main.Gridlang;

public class P3_AllNegativesToZero extends Problem3{

	@Override
	public int[] process(int[] input, int param) {
		int[] out = new int[input.length];
		for (int i=0;i<out.length;i++) {
			if (input[i] < 0) {
				out[i] = 0;
			}else {
				out[i] = input[i];
			}
		}
		return out;
	}

	@Override
	public int[][] getGridlang(int nLines) {
		ArrayList<int[]> lines = new ArrayList<int[]>();
		lines.add(new int[] {Gridlang.OP_LOOP,2,0,0,0});//iterator to 2
		lines.add(new int[] {Gridlang.OP_ARRAY_TO_VAR_ASSIGN,0,2,3,0});//var_3 = in[var_2]
		lines.add(new int[] {Gridlang.OP_COND, 3,0,0,0});//If var_3 < 0
		lines.add(new int[] {Gridlang.OP_ELSEBLOCK, 0,0,0,0});//else
		lines.add(new int[] {Gridlang.OP_VAR_TO_LITERAL,3,0,0,0});//var_3 = 0
		lines.add(new int[] {Gridlang.OP_ENDBLOCK,0,0,0,0});//Endif
		lines.add(new int[] {Gridlang.OP_VAR_TO_ARRAY_ASSIGN,1,2,3,0});//out[var_3] = in[var_2]
		lines.add(new int[] {Gridlang.OP_ENDBLOCK,0,0,0,0});//Endloop
		while(lines.size() < nLines) {lines.add(new int[] {0,0,0,0,0});}
		
		int[][] program = new int[nLines][5];
		for (int i=0;i<program.length;i++) {
			for (int j=0;j<5;j++) {
				program[i][j] = lines.get(i)[j];
			}
		}
		return program;
	}

}
