package problems;

public class P_Unleaf extends Problem{

	@Override
	public int[] run(int[] input,int param) {
		int[] reply = new int[input.length];
		int halfway = input.length/2;
		for (int i=0;i<halfway;i++) {
			reply[i] = input[i*2];
		}
		for (int i=0;i<halfway;i++) {
			reply[i+halfway] = input[(i*2) + 1];
		}
		return reply;
	}

}
