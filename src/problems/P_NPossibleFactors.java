package problems;

public class P_NPossibleFactors extends Problem{

	@Override
	public int[] run(int[] input,int param) {
		int[] reply = new int[input.length];
		for (int i=0;i<reply.length;i++) {
			int factors = 0;
			for (int j=1;j<input[i];j++) {
				if (input[i] % j == 0) {
					factors += 1;
				}
			}
			reply[i] = factors;
		}
		return reply;
	}

	
	@Override
	public int[] asSimple(int[] input,int param) {
		int[] reply;

		int i;
		int j;
		int hold;
		int factors;
		int inputLen;
		boolean cond;
		
		inputLen = input.length;
		reply = new int[inputLen];

		i = 0;
		cond = i < inputLen;
		while(cond) {
			factors = 0;
			j = 1;
			cond = j < input[i];
			while(cond) {
				hold = input[i] % j;
				cond = hold == 0;
				if (cond) {
					factors = factors + 1;
				}
 				j = j + 1;
				cond = j < input[i];
			}
			reply[i] = factors;
			
			i = i + 1;
			cond = i < inputLen;
		}
		
		return reply;
	}
}
