package problems;

public class P_Sort extends Problem{
	public int[] run(int[] input,int param) {
		int[] reply = new int[input.length];

		for (int i=0;i<input.length;i++) {reply[i] = input[i];}
		for (int i=0;i<reply.length;i++) {
			for (int j=0;j<reply.length-1;j++) {
				if (reply[j] < reply[j+1]) {
					int swap = reply[j];
					reply[j] = reply[j+1];
					reply[j+1] = swap;
				}
			}
		}
		
		return reply;
	}
	
	@Override
	public int[] asSimple(int[] input,int param) {
		int[] reply;

		int j;
		int i;
		int k;
		int inputLen;
		int shortLen;
		int swap;
		boolean cond;
		
		inputLen = input.length;
		shortLen = inputLen -1;
		reply = new int[inputLen];

		i = 0;
		cond = i < inputLen;
		while(cond) {
			reply[i] = input[i];
			i = i + 1;
			cond = i < inputLen;
		}

		i = 0;
		cond = i < inputLen;
		while(cond) {
			j = 0;
			cond = j < shortLen;
			while(cond) {
				k = j + 1;
				cond = reply[j] < reply[k];
				if (cond) {
					swap = reply[j];
					reply[j] = reply[k];
					reply[k] = swap;
				}
				
				j = j + 1;
				cond = j < shortLen;
			}
			
			i = i + 1;
			cond = i < inputLen;
		}
		return reply;
	}
}
