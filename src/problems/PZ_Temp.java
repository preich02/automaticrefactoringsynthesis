package problems;

public class PZ_Temp extends Problem{

	@Override
	public int[] run(int[] input, int param) {
		P_Sort sorter = new P_Sort();
		return sorter.run(input, param);
	}


	@Override
	public int[] asSimple(int[] input,int param) {
		int[] reply;

		int j;
		int i;
		int k;
		int inputLen;
		int swap;
		boolean cond;
		
		inputLen = input.length;
		reply = new int[inputLen];

		i = 0;
		cond = i < inputLen;
		while(cond) {
			reply[i] = input[i];
			i = i + 1;
			cond = i < inputLen;
		}

		i = 0;
		cond = i < inputLen;
		while(cond) {
			j = i;
			cond = j > 0;
			while(cond) {
				k = j - 1;
				cond = reply[j] > reply[k];
				if (cond) {
					swap = reply[j];
					reply[j] = reply[k];
					reply[k] = swap;
				}
				
				j = j - 1;
				cond = j > 0;
			}
			
			i = i + 1;
			cond = i < inputLen;
		}
		return reply;
	}
}
