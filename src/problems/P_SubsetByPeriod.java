package problems;

public class P_SubsetByPeriod extends Problem{

	@Override
	public int[] run(int[] input, int param) {
		int[] reply = new int[input.length];
		int q = 0;
		for (int i=0;i<reply.length;i++) {
			if (i % param == 0) {
				reply[q] = input[i];
				q += 1;
			}
		}
		return reply;
	}

}
