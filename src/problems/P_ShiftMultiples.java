package problems;

public class P_ShiftMultiples extends Problem{

	@Override
	public int[] run(int[] input,int param) {
		int[] reply = new int[input.length];
		int q = 0;
		for (int i=0;i<input.length;i++) {
			if (input[i] % param == 0) {
				reply[q] = input[i];
				q += 1;
			}
		}
		for (int i=0;i<input.length;i++) {
			if (input[i] % param != 0) {
				reply[q] = input[i];
				q += 1;
			}
		}
		return reply;
	}

}
 