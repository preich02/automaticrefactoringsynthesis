package problems;

public class P_Mode extends Problem{

	@Override
	public int[] run(int[] input,int param) {
		int[] reply = new int[input.length];
		int best = input[0];
		int max = -1;
		for (int i=0;i<reply.length;i++) {
			int count = 0;
			for (int j=0;j<reply.length;j++) {
				if (input[i] == input[j]) {
					count += 1;
				}
			}
			if (count > max) {
				max = count;
				best = input[i];
			}
		}
		for (int i=0;i<reply.length;i++) {
			reply[i] = best;
		}
		
		return reply;
	}

	@Override
	public int[] asSimple(int[] input,int param) {
		int[] reply;

		int i;
		int j;
		int best;
		int max;
		int count;
		int inputLen;
		boolean cond;
		
		inputLen = input.length;
		max = 0;
		best = 0;
		reply = new int[inputLen];

		i = 0;
		cond = i < inputLen;
		while(cond) {
			count = 0;
			j =0;
			cond = j < inputLen;
			while(cond) {
				cond = input[i] == input[j];
				if (cond) {
					count = count + 1;
				}
 				j = j + 1;
				cond = j < inputLen;
			}
			cond = count > max;
			if (cond) {
				max = count;
				best = input[i];
			}
			
			i = i + 1;
			cond = i < inputLen;
		}
		i = 0;
		cond = i < inputLen;
		while(cond) {
			reply[i] = best;
			i = i + 1;
			cond = i < inputLen;
		}
		
		return reply;
	}
}
