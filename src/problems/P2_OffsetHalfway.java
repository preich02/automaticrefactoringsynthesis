package problems;

public class P2_OffsetHalfway extends Problem2{

	@Override
	public int[] run(int[] input) {
		int[] reply = new int[input.length];
		for (int i=0;i<input.length;i++) {
			int index = (i + input.length/2) % input.length;
			reply[i] = input[index];
		}
		return reply;
	}
}
