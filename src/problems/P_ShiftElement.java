package problems;

public class P_ShiftElement extends Problem{

	@Override
	public int[] run(int[] input,int param) {
		int[] reply = new int[input.length];
		int mobile = input[param];
		for (int i=0;i<param;i++) {
			reply[i] = input[i];
		}
		for (int i=param+1;i<input.length;i++) {
			reply[i-1] = input[i];
		}
		reply[reply.length-1] = mobile;
		return reply;
	}

}
 