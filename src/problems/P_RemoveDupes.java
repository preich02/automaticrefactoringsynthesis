package problems;

import main.Utils;

public class P_RemoveDupes extends Problem{

	@Override
	public int[] randomInput(int len) {
		int[] reply = new int[len];
		for (int i=0;i<reply.length;i++) {
			reply[i] = Utils.random.nextInt(8);
		}
		return reply;
	}
	
	@Override
	public int[] run(int[] input,int param) {
		int[] reply = input.clone();
		for (int i=0;i<reply.length;i++) {
			int v = input[i];
			for (int j=i+1;j<reply.length;j++) {
				if (v == input[j]) {
					reply[j] = 0;
					reply[i] = 0;
				}
			}
		}
		return reply;
	}

}
 