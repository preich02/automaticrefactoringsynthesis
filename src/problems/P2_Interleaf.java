package problems;

public class P2_Interleaf extends Problem2{

	@Override
	public int[] run(int[] input) {
		int[] reply = new int[input.length];
		int halfway = input.length/2;
		for (int i=0;i<halfway;i++) {
			reply[i*2] = input[i];
		}
		for (int i=0;i<halfway;i++) {
			reply[i*2 + 1] = input[i+halfway];
		}
		return reply;
	}
}
