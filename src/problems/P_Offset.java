package problems;

public class P_Offset extends Problem{

	@Override
	public int[] run(int[] input,int param) {
		int[] reply = new int[input.length];
		for (int i=0;i<input.length;i++) {
			int index = (i + param) % input.length;
			reply[i] = input[index];
		}
		return reply;
	}

	@Override
	public int[] asSimple(int[] input,int param) {
		int[] reply;
		int index;
		int inputLen;
		int i;
		boolean cond;
		
		i = 0;
		inputLen = input.length;
		reply = new int[inputLen];
		
		
		i = 0;
		cond = i < inputLen;
		while(cond) {
			index = i + param;
			index = index % inputLen;
			reply[i] = input[index];
			
			i = i + 1;
			cond = i < inputLen;
		}
		
		return reply;
	}

}
