package problems;

public class P_ZeroN extends Problem{

	@Override
	public int[] run(int[] input, int param) {
		int[] reply = new int[input.length];
		for (int i=0;i<reply.length;i++) {
			if (i < param) {
				reply[i] = 0;
			}else {
				reply[i] = input[i];
			}
		}
		return input;
	}

}
