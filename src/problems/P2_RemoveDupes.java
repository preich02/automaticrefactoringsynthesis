package problems;

import main.Utils;

public class P2_RemoveDupes extends Problem2{
	@Override
	public int[] run(int[] input) {
		int[] reply = input.clone();
		for (int i=0;i<reply.length;i++) {
			int v = input[i];
			for (int j=i+1;j<reply.length;j++) {
				if (v == input[j]) {
					reply[j] = 0;
					reply[i] = 0;
				}
			}
		}
		return reply;
	}

}
 